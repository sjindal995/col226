open TextIO;
Control.Print.printDepth := 10000;


exception invalid
exception FailUnify

val symbols : string list ref = ref [];
val arities : string list ref = ref [];
datatype term = NODE of string * term list
     			   | LEAF of string
     			   | VAR of string
     			   | NONE

fun check_sig name = 
	let 
		val inp = openIn(name);
		val str = ref "";
		val i = ref 0;
		val j = ref 0;
		val l = ref [];
		val valid = ref true;
		
	in
		symbols := [];
		arities := [];
		str := input(inp);
		l := String.tokens (fn x => ((x = #" ") orelse (x = #",") orelse (x = #"(") orelse (x = #")") orelse (x = #"[") orelse (x = #"]") orelse (x = #"\"") ))  (!str) ;
		
		while ((!i) < (length (!l))) do
			(
				if (((!i) mod 2) = 0 ) then (symbols := [(List.nth(!l, !i))]@(!symbols))
				else (arities := [(List.nth(!l, !i))]@(!arities));
				i := (!i)+1
			);
			i := 0;
		while ((!i) < (length (!symbols))) do
			(
				if (valOf(Int.fromString(List.nth(!arities, !i))) < 0) then ((valid := false); (j := length (!symbols)); (i := length (!symbols)))
				else (j := (!i) + 1);

				while ((!j) < (length (!symbols))) do
				(
					if ((List.nth(!symbols, !i) = List.nth(!symbols, !j)) andalso (List.nth(!arities, !i) <> List.nth(!arities, !j))) then ((valid := false); (j := (length (!symbols))); (i := length (!symbols)))
					else (j := (!j)+1);
					j := (!j)+1
					
				);
				i := (!i) + 1
			);
			!valid
	end


fun searcharity ([],x,_) = raise invalid
  | searcharity (y::ls,x,i) = if (y = x) then (valOf(Int.fromString(List.nth(!arities,!i)))) else (i := (!i)+1;searcharity(ls,x,i))

fun wff name h =
(
	let
	val valid = ref true;
fun	help (VAR(x)) = (if (searcharity(!symbols,x,ref 0) = 0) then true else (raise invalid))
  	  | help (LEAF(x)) = (if (searcharity(!symbols,x,ref 0) = 0) then true else (raise invalid))
      | help (NODE(x,l)) = (if (searcharity(!symbols,x,ref 0) = (length l)) then ((List.map help l);true ) else (raise invalid ))
	in
		valid := (check_sig name);
		if((!valid) = true)then (help h) else (raise invalid)
	end
)

fun tablesearch ([]) (x) = 0
  | tablesearch ((a,_)::ls) (x) = (if (a=x) then 1 else (tablesearch ls x))

fun tablereturn ([] : (term * term) list) x = raise invalid 		(* INSERT EXCEPTION !!!!!!!!!!!!!!!!!!!*)
  | tablereturn ((a,b)::ls : (term * term) list) x = if (a=x) then (b : term) else (tablereturn ls x)

fun subst s (VAR(x)) = if (tablesearch s (VAR(x)) = 1) then ((tablereturn s (VAR(x)))) else (VAR(x)) 
  | subst s (LEAF(x)) = (LEAF(x))
  | subst s (NODE(x,l)) = (NODE(x,(List.map (subst s) l)))


fun searchright (VAR(x)) (VAR(y)) = if (x = y) then (1) else 0
  | searchright (VAR(x)) (NODE(y,l)) = (foldl (fn(a,r)=>(searchright (VAR(x)) a) + r) 0 l)
  | searchright (VAR(x)) (LEAF(y)) = 0

fun lookup (VAR(x)) (LEAF(y))  = []
  | lookup (VAR(x)) (VAR(y)) = if (x=y) then [VAR(y)] else []
  | lookup (VAR(x)) (NODE(y,l)) = List.concat (List.map (lookup (VAR(x))) l)


fun common ([],b,ans1) = ans1
  | common ((x1,x2)::y,b,ans1) = common(y,b,ans1 @ [(x1,(subst b x2))])

fun mgu (VAR(x)) (VAR(y)) = (if (x=y) then [] else [(VAR(x),VAR(y))])
  | mgu (VAR(x)) (LEAF(y)) = ([(VAR(x),LEAF(y))])
  | mgu (LEAF(x)) (LEAF(y)) = (if (x=y) then [] else (print "leaf and leaf\n" ;raise FailUnify))				(* INSERT EXCEPTION!!!!!!!!!!!!!!!!!!!!*)
  | mgu (VAR(x)) (NODE(y,l)) = (if ((searchright (VAR(x)) (NODE(y,l))) <> 0) then (print "var and node\n" ;raise FailUnify) else [(VAR(x),NODE(y,l))])			(* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  | mgu (LEAF(x)) (NODE(y,l)) = (print "leaf and node\n"; raise FailUnify) 		(* INSERT EXCEPTION !!!!!!!!!!!!*)
  | mgu (NODE(x,l1)) (NODE(y,l2)) = 
  (
  	let
		val i = ref 0;
		val rhos = ref [];
		val currho = ref [];
	in
		if (x <> y) then (print "node and node" ;raise FailUnify) else 
		(
			while( (!i) < (length l1)) do
			(
				currho := mgu (subst (!rhos) (List.nth(l1,(!i)))) (subst (!rhos) (List.nth(l2,(!i))));
				rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
				i := (!i)+1
			);
			(!rhos)
		)
	end
  )
  | mgu x y = (mgu y x)

fun mgulist termlist = 
	let
		val i = ref 0;
		val rhos = ref [];
		val currho = ref[];

	in
		if ((length termlist) < 2) then [] else(
			while((!i) < ((length termlist)-1)) do
			(
				currho := mgu (subst (!rhos) (List.nth(termlist,(!i)))) (subst (!rhos) (List.nth(termlist,(!i)+1)));
				rhos := ((common((!rhos),(!currho),[]))@(!currho));
				i := (!i)+1
			);

			(!rhos)
		)
	end

val t1 = NODE ("add",[VAR "y",VAR "z"]);
val t2 = NODE("add",[VAR "z",VAR "a"]);
val t3 = NODE("add",[VAR "a",LEAF "1"]);
