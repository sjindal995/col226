signature WELLFORM =
  sig
     datatype term = NODE of string * term list
     			   | LEAF of string
     			   | VAR of string
  end

structure Wellform :> WELLFORM =
   struct
     datatype term = NODE of string * term list
     			   | LEAF of string
     			   | VAR of string
   end