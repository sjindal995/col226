structure Tokens = Tokens

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val eof = fn () => Tokens.EOF(!line,!line)
fun makeInt (s : string) = s
val strsize : int ref = ref 0;


%%
%header (functor FolLexFun(structure Tokens: Fol_TOKENS
			   structure Interface: INTERFACE) : LEXER);
ints = [1-9][0-9]*;
lcid = [a-z]([A-Za-z'_] | [0-9])*;
ucid = [A-Z]([A-Za-z'_] | [0-9])*;
ws = [\ \t];
digit = [0-9];
nzdigits = [1-9];
frac = ("."{digit}*{nzdigits}) | ".0" ;
expo = "E"((~?){ints} | "0");
reals = {ints}{frac}{expo}?;
%%
{ws}	=> (lex());
\n	=> (next_line(); lex());

"+"      => (Tokens.PLUS(!line,!line));
"-"      => (Tokens.SUB(!line,!line));
"*"      => (Tokens.TIMES(!line,!line));
"div"	=> (Tokens.INTDIV(!line,!line));
"/"      => (Tokens.REALDIV(!line,!line));
"%"  => (Tokens.MOD(!line,!line));
"abs" => (Tokens.ABS(!line,!line));
">" => (Tokens.GREAT(!line,!line));
"<" => (Tokens.LESS(!line,!line));
">=" => (Tokens.GREATEQ(!line,!line));
"<=" => (Tokens.LESSEQ(!line,!line));
"==" => (Tokens.EQ(!line,!line));
"<>" => (Tokens.NEQ(!line,!line));
"if" => (Tokens.IF(!line,!line));
"then" => (Tokens.THEN(!line,!line));
"else" => (Tokens.ELSE(!line,!line));
"let" => (Tokens.LET(!line,!line));
"end" => (Tokens.END(!line,!line));
"," => (Tokens.COMMA(!line,!line));
":" => (Tokens.COLON(!line,!line));
"&" => (Tokens.CONJUNCT(!line,!line));
"||" => (Tokens.DISJUNCT(!line,!line));
"~" => (Tokens.NEGATION(!line,!line));
"forall" => (Tokens.FORALL(!line,!line));
"exists" => (Tokens.EXISTS(!line,!line));
"." => (Tokens.DOT(!line,!line));


"exp" => (Tokens.EXPONENT(!line,!line));

("~")?{ints}	=> (Tokens.NUM (valOf(Int.fromString(yytext)),!line,!line));
"0"	=> (Tokens.NUM (0,!line,!line));

"true"	=> (Tokens.BOOL (true,!line,!line));
"false" => (Tokens.BOOL (false,!line,!line));
"\""[^"\""]*"\"" => ((strsize := ((size yytext)-2)) ; (Tokens.STRING (String.substring(yytext,1,(size yytext)-2),!line,!line)));
"^" => (Tokens.CAT(!line,!line));
"["({ints}|"0") => (Tokens.SUBLEFT (foldl (fn(a,r)=>ord(a)-ord(#"0")+10*r) 0 (explode (String.substring(yytext,1,(size yytext)-1))),!line,!line));
".."({ints}|"0")"]" => (Tokens.SUBRIGHT (foldl (fn(a,r)=>ord(a)-ord(#"0")+10*r) 0 (explode (String.substring(yytext,2,(size yytext)-3))),!line,!line));
"...]" => (Tokens.SUBRIGHT ((!strsize) - 1,!line,!line));


"(" => (Tokens.LPAREN(!line,!line));
")" => (Tokens.RPAREN(!line,!line));

"0.0" => (Tokens.REAL (0.0,!line,!line));
("~")?{reals} => (Tokens.REAL (valOf(Real.fromString(yytext)),!line,!line));
{lcid} => (Tokens.SMALLID (yytext,!line,!line));
{ucid} => (Tokens.CAPID (yytext,!line,!line));
.	=> (error ("ignoring illegal character" ^ yytext, !line,!line); lex());
