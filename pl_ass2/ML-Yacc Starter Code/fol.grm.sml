
functor FolLrValsFun (structure Token : TOKEN
			       structure Absyn : ABSYN ) : Fol_LRVALS = 
struct
structure ParserData=
struct
structure Header = 
struct

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\016\000\011\000\000\000\
\\001\000\017\000\028\000\018\000\027\000\029\000\026\000\032\000\025\000\000\000\
\\001\000\017\000\028\000\018\000\027\000\029\000\026\000\032\000\025\000\
\\038\000\024\000\039\000\023\000\000\000\
\\001\000\021\000\012\000\000\000\
\\001\000\021\000\032\000\000\000\
\\001\000\022\000\045\000\000\000\
\\001\000\022\000\052\000\000\000\
\\001\000\023\000\013\000\000\000\
\\001\000\036\000\000\000\037\000\000\000\000\000\
\\001\000\037\000\046\000\000\000\
\\001\000\040\000\003\000\000\000\
\\054\000\000\000\
\\055\000\000\000\
\\056\000\016\000\011\000\000\000\
\\057\000\000\000\
\\058\000\000\000\
\\059\000\000\000\
\\060\000\000\000\
\\061\000\000\000\
\\062\000\028\000\044\000\000\000\
\\063\000\000\000\
\\064\000\037\000\014\000\000\000\
\\065\000\000\000\
\\066\000\000\000\
\\067\000\028\000\047\000\000\000\
\\068\000\000\000\
\\069\000\001\000\043\000\002\000\042\000\003\000\041\000\004\000\040\000\
\\005\000\039\000\006\000\038\000\007\000\037\000\008\000\036\000\
\\031\000\035\000\000\000\
\\070\000\000\000\
\\071\000\000\000\
\\072\000\000\000\
\\073\000\001\000\043\000\002\000\042\000\003\000\041\000\004\000\040\000\
\\005\000\039\000\006\000\038\000\007\000\037\000\008\000\036\000\
\\031\000\035\000\000\000\
\\074\000\001\000\043\000\002\000\042\000\003\000\041\000\004\000\040\000\
\\005\000\039\000\006\000\038\000\007\000\037\000\008\000\036\000\
\\031\000\035\000\000\000\
\\075\000\000\000\
\\076\000\000\000\
\\077\000\000\000\
\\078\000\000\000\
\\079\000\000\000\
\\080\000\000\000\
\\081\000\000\000\
\\082\000\000\000\
\\083\000\000\000\
\\084\000\000\000\
\\085\000\000\000\
\\086\000\000\000\
\\087\000\000\000\
\\088\000\000\000\
\\089\000\000\000\
\"
val actionRowNumbers =
"\010\000\013\000\003\000\007\000\
\\015\000\021\000\014\000\013\000\
\\011\000\025\000\002\000\000\000\
\\016\000\012\000\004\000\001\000\
\\029\000\028\000\026\000\019\000\
\\005\000\046\000\045\000\035\000\
\\034\000\032\000\033\000\022\000\
\\009\000\024\000\002\000\030\000\
\\001\000\041\000\044\000\043\000\
\\042\000\040\000\039\000\038\000\
\\037\000\036\000\002\000\017\000\
\\020\000\000\000\006\000\031\000\
\\018\000\023\000\027\000\008\000"
val gotoT =
"\
\\018\000\051\000\000\000\
\\001\000\008\000\002\000\007\000\003\000\006\000\004\000\005\000\
\\006\000\004\000\007\000\003\000\010\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\013\000\002\000\007\000\003\000\006\000\004\000\005\000\
\\006\000\004\000\007\000\003\000\010\000\002\000\000\000\
\\000\000\
\\000\000\
\\005\000\020\000\011\000\019\000\012\000\018\000\013\000\017\000\
\\014\000\016\000\015\000\015\000\017\000\014\000\000\000\
\\004\000\029\000\008\000\028\000\009\000\027\000\010\000\002\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\012\000\031\000\013\000\017\000\014\000\016\000\015\000\015\000\000\000\
\\000\000\
\\000\000\
\\016\000\032\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\046\000\011\000\019\000\012\000\018\000\013\000\017\000\
\\014\000\016\000\015\000\015\000\017\000\014\000\000\000\
\\016\000\032\000\000\000\
\\012\000\047\000\013\000\017\000\014\000\016\000\015\000\015\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\048\000\011\000\019\000\012\000\018\000\013\000\017\000\
\\014\000\016\000\015\000\015\000\017\000\014\000\000\000\
\\000\000\
\\000\000\
\\004\000\029\000\009\000\049\000\010\000\002\000\000\000\
\\000\000\
\\016\000\032\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 52
val numrules = 36
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | SUBRIGHT of  (int)
 | SUBLEFT of  (int) | NUM of  (int) | CAPID of  (string)
 | SMALLID of  (string) | STRING of  (string) | REAL of  (real)
 | BOOL of  (bool) | start of  (Absyn.absyn)
end
type svalue = MlyValue.svalue
type result = Absyn.absyn
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
(nil
,nil
 $$ (T 36))::
nil
val noShift = 
fn _ => false
val showTerminal =
fn (T 0) => "PLUS"
  | (T 1) => "SUB"
  | (T 2) => "TIMES"
  | (T 3) => "INTDIV"
  | (T 4) => "REALDIV"
  | (T 5) => "GREAT"
  | (T 6) => "LESS"
  | (T 7) => "GREATEQ"
  | (T 8) => "LESSEQ"
  | (T 9) => "EQ"
  | (T 10) => "NEQ"
  | (T 11) => "CAT"
  | (T 12) => "BOOL"
  | (T 13) => "REAL"
  | (T 14) => "STRING"
  | (T 15) => "SMALLID"
  | (T 16) => "CAPID"
  | (T 17) => "NUM"
  | (T 18) => "SUBLEFT"
  | (T 19) => "SUBRIGHT"
  | (T 20) => "LPAREN"
  | (T 21) => "RPAREN"
  | (T 22) => "IF"
  | (T 23) => "THEN"
  | (T 24) => "ELSE"
  | (T 25) => "LET"
  | (T 26) => "END"
  | (T 27) => "COMMA"
  | (T 28) => "NEGATION"
  | (T 29) => "COLON"
  | (T 30) => "MOD"
  | (T 31) => "ABS"
  | (T 32) => "EXPONENT"
  | (T 33) => "CONJUNCT"
  | (T 34) => "DISJUNCT"
  | (T 35) => "EOF"
  | (T 36) => "DOT"
  | (T 37) => "FORALL"
  | (T 38) => "EXISTS"
  | (T 39) => "PARSEPROG"
  | (T 40) => "PARSEQUERY"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 40) $$ (T 39) $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34)
 $$ (T 33) $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27)
 $$ (T 26) $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20)
 $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ 
(T 4) $$ (T 3) $$ (T 2) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( _, _, program1right)) :: ( _, ( _, PARSEPROG1left, _
)) :: rest671)) => let val  result = MlyValue.start (Absyn.null)
 in ( LrTable.NT 17, ( result, PARSEPROG1left, program1right), rest671
)
end
|  ( 1, ( ( _, ( _, _, program1right)) :: ( _, ( _, declaration1left,
 _)) :: rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 0, ( result, declaration1left, program1right), 
rest671)
end
|  ( 2, ( rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 0, ( result, defaultPos, defaultPos), rest671)
end
|  ( 3, ( ( _, ( _, fact1left, fact1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 1, ( result, fact1left, fact1right), rest671)
end
|  ( 4, ( ( _, ( _, rule1left, rule1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 1, ( result, rule1left, rule1right), rest671)
end
|  ( 5, ( ( _, ( _, _, DOT1right)) :: ( _, ( _, predicate1left, _)) ::
 rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 2, ( result, predicate1left, DOT1right), rest671)
end
|  ( 6, ( ( _, ( _, _, RPAREN1right)) :: _ :: _ :: ( _, ( _, 
predicatesymbol1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID ()
 in ( LrTable.NT 3, ( result, predicatesymbol1left, RPAREN1right), 
rest671)
end
|  ( 7, ( ( _, ( _, _, termsequence1right)) :: _ :: ( _, ( _, 
term1left, _)) :: rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 4, ( result, term1left, termsequence1right), rest671)

end
|  ( 8, ( ( _, ( _, term1left, term1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 4, ( result, term1left, term1right), rest671)
end
|  ( 9, ( ( _, ( _, _, DOT1right)) :: _ :: _ :: ( _, ( _, head1left, _
)) :: rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 5, ( result, head1left, DOT1right), rest671)
end
|  ( 10, ( ( _, ( _, predicate1left, predicate1right)) :: rest671)) =>
 let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 6, ( result, predicate1left, predicate1right), 
rest671)
end
|  ( 11, ( ( _, ( _, predicatesequence1left, predicatesequence1right))
 :: rest671)) => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 7, ( result, predicatesequence1left, 
predicatesequence1right), rest671)
end
|  ( 12, ( ( _, ( _, _, predicatesequence1right)) :: _ :: ( _, ( _, 
predicate1left, _)) :: rest671)) => let val  result = MlyValue.ntVOID
 ()
 in ( LrTable.NT 8, ( result, predicate1left, predicatesequence1right)
, rest671)
end
|  ( 13, ( ( _, ( _, predicate1left, predicate1right)) :: rest671)) =>
 let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 8, ( result, predicate1left, predicate1right), 
rest671)
end
|  ( 14, ( ( _, ( _, SMALLID1left, SMALLID1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 9, ( result, SMALLID1left, SMALLID1right), rest671)

end
|  ( 15, ( ( _, ( _, expression1left, expression1right)) :: rest671))
 => let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 10, ( result, expression1left, expression1right), 
rest671)
end
|  ( 16, ( ( _, ( _, _, RPAREN1right)) :: _ :: _ :: ( _, ( _, 
functionsymbol1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID ()
 in ( LrTable.NT 10, ( result, functionsymbol1left, RPAREN1right), 
rest671)
end
|  ( 17, ( ( _, ( _, constant1left, constant1right)) :: rest671)) =>
 let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 11, ( result, constant1left, constant1right), rest671
)
end
|  ( 18, ( ( _, ( _, variable1left, variable1right)) :: rest671)) =>
 let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 11, ( result, variable1left, variable1right), rest671
)
end
|  ( 19, ( ( _, ( _, _, expression1right)) :: ( _, ( _, 
unaryoperator1left, _)) :: rest671)) => let val  result = 
MlyValue.ntVOID ()
 in ( LrTable.NT 11, ( result, unaryoperator1left, expression1right), 
rest671)
end
|  ( 20, ( ( _, ( _, _, expression2right)) :: _ :: ( _, ( _, 
expression1left, _)) :: rest671)) => let val  result = MlyValue.ntVOID
 ()
 in ( LrTable.NT 11, ( result, expression1left, expression2right), 
rest671)
end
|  ( 21, ( ( _, ( _, NUM1left, NUM1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 12, ( result, NUM1left, NUM1right), rest671)
end
|  ( 22, ( ( _, ( _, CAPID1left, CAPID1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 13, ( result, CAPID1left, CAPID1right), rest671)
end
|  ( 23, ( ( _, ( _, NEGATION1left, NEGATION1right)) :: rest671)) =>
 let val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 14, ( result, NEGATION1left, NEGATION1right), rest671
)
end
|  ( 24, ( ( _, ( _, ABS1left, ABS1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 14, ( result, ABS1left, ABS1right), rest671)
end
|  ( 25, ( ( _, ( _, PLUS1left, PLUS1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, PLUS1left, PLUS1right), rest671)
end
|  ( 26, ( ( _, ( _, SUB1left, SUB1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, SUB1left, SUB1right), rest671)
end
|  ( 27, ( ( _, ( _, TIMES1left, TIMES1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, TIMES1left, TIMES1right), rest671)
end
|  ( 28, ( ( _, ( _, INTDIV1left, INTDIV1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, INTDIV1left, INTDIV1right), rest671)

end
|  ( 29, ( ( _, ( _, REALDIV1left, REALDIV1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, REALDIV1left, REALDIV1right), rest671)

end
|  ( 30, ( ( _, ( _, MOD1left, MOD1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, MOD1left, MOD1right), rest671)
end
|  ( 31, ( ( _, ( _, GREAT1left, GREAT1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, GREAT1left, GREAT1right), rest671)
end
|  ( 32, ( ( _, ( _, LESS1left, LESS1right)) :: rest671)) => let val  
result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, LESS1left, LESS1right), rest671)
end
|  ( 33, ( ( _, ( _, GREATEQ1left, GREATEQ1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 15, ( result, GREATEQ1left, GREATEQ1right), rest671)

end
|  ( 34, ( ( _, ( _, FORALL1left, FORALL1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 16, ( result, FORALL1left, FORALL1right), rest671)

end
|  ( 35, ( ( _, ( _, EXISTS1left, EXISTS1right)) :: rest671)) => let
 val  result = MlyValue.ntVOID ()
 in ( LrTable.NT 16, ( result, EXISTS1left, EXISTS1right), rest671)

end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.start x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Fol_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun SUB (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun INTDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun REALDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun GREAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun LESS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun GREATEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LESSEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun CAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun BOOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.BOOL i,p1,p2))
fun REAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.REAL i,p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.STRING i,p1,p2))
fun SMALLID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.SMALLID i,p1,p2))
fun CAPID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.CAPID i,p1,p2))
fun NUM (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.NUM i,p1,p2))
fun SUBLEFT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.SUBLEFT i,p1,p2))
fun SUBRIGHT (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.SUBRIGHT i,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun NEGATION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun MOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun ABS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun EXPONENT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun CONJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun DISJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun DOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun FORALL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun EXISTS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun PARSEPROG (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.VOID,p1,p2))
fun PARSEQUERY (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(
ParserData.MlyValue.VOID,p1,p2))
end
end
