structure Tokens = Tokens

type pos = int
type svalue = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult= (svalue,pos) token

val pos = ref 0
val strsize : int ref = ref 0;
val eof = fn () => Tokens.EOF(!pos,!pos)
fun error (e,l,_) = TextIO.output (TextIO.stdOut, String.concat[
	"line ", (Int.toString l), ": ", e, "\n"])


%%
%header (functor CalcLexFun(structure Tokens: Calc_TOKENS));
ints = [1-9][0-9]*;
lcid = [a-z]([A-Za-z'_] | [0-9])*;
ucid = [A-Z]([A-Za-z'_] | [0-9])*;
ws = [\ \t];
digit = [0-9];
nzdigits = [1-9];
frac = ("."{digit}*{nzdigits}) | ".0" ;
expo = "E"((~?){ints} | "0");
reals = {ints}{frac}{expo}?;

%%

\n       => (pos := (!pos) + 1; lex());
{ws}+    => (lex());

"+"      => (Tokens.PLUS(!pos,!pos));
"-"      => (Tokens.SUB(!pos,!pos));
"*"      => (Tokens.TIMES(!pos,!pos));
"div"	=> (Tokens.INTDIV(!pos,!pos));
"/"      => (Tokens.REALDIV(!pos,!pos));
"%"  => (Tokens.MOD(!pos,!pos));
"abs" => (Tokens.ABS(!pos,!pos));
">" => (Tokens.GREAT(!pos,!pos));
"<" => (Tokens.LESS(!pos,!pos));
">=" => (Tokens.GREATEQ(!pos,!pos));
"<=" => (Tokens.LESSEQ(!pos,!pos));
"==" => (Tokens.EQ(!pos,!pos));
"<>" => (Tokens.NEQ(!pos,!pos));
":-" => (Tokens.IF(!pos,!pos));
"then" => (Tokens.THEN(!pos,!pos));
"else" => (Tokens.ELSE(!pos,!pos));
"let" => (Tokens.LET(!pos,!pos));
"end" => (Tokens.END(!pos,!pos));
"," => (Tokens.COMMA(!pos,!pos));
":" => (Tokens.COLON(!pos,!pos));
"&" => (Tokens.CONJUNCT(!pos,!pos));
"||" => (Tokens.DISJUNCT(!pos,!pos));
"." => (Tokens.DOT(!pos,!pos));
"?-" => (Tokens.QUES(!pos,!pos));
"[" => (Tokens.LSBRACKET(!pos,!pos));
"]" => (Tokens.RSBRACKET(!pos,!pos));
"|" => (Tokens.LISTBAR(!pos,!pos));

"exp" => (Tokens.EXPONENT(!pos,!pos));

("~")?{ints}	=> (Tokens.NUM (valOf(Int.fromString(yytext)),!pos,!pos));
"0"	=> (Tokens.NUM (0,!pos,!pos));

"true"	=> (Tokens.BOOL (true,!pos,!pos));
"false" => (Tokens.BOOL (false,!pos,!pos));
"\""[^"\""]*"\"" => ((strsize := ((size yytext)-2)) ; (Tokens.STRING (String.substring(yytext,1,(size yytext)-2),!pos,!pos)));
"^" => (Tokens.CAT(!pos,!pos));

"(" => (Tokens.LPAREN(!pos,!pos));
")" => (Tokens.RPAREN(!pos,!pos));

"0.0" => (Tokens.REAL (0.0,!pos,!pos));
("~")?{reals} => (Tokens.REAL (valOf(Real.fromString(yytext)),!pos,!pos));
{lcid} => (Tokens.SMALLID (yytext,!pos,!pos));
{ucid} => (Tokens.CAPID (yytext,!pos,!pos));

"~" => (Tokens.NEGATION(!pos,!pos));
";" => (Tokens.SEMI (!pos,!pos));
.	=> (error ("ignoring illegal character " ^ yytext, !pos,!pos); lex());
