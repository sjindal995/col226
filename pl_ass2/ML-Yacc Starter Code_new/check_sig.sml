open TextIO;
Control.Print.printDepth := 10000;

use "datatype.sml";
exception invalid
exception FailUnify

val symbols : string list ref = ref [];
val arities : string list ref = ref [];

fun check_sig name = 
	let 
		val inp = openIn(name);
		val str = ref "";
		val i = ref 0;
		val j = ref 0;
		val l = ref [];
		val valid = ref true;
		
	in
		symbols := [];
		arities := [];
		str := input(inp);
		l := String.tokens (fn x => ((x = #" ") orelse (x = #",") orelse (x = #"(") orelse (x = #")") orelse (x = #"[") orelse (x = #"]") orelse (x = #"\"") ))  (!str) ;
		
		while ((!i) < (length (!l))) do
			(
				if (((!i) mod 2) = 0 ) then (symbols := [(List.nth(!l, !i))]@(!symbols))
				else (arities := [(List.nth(!l, !i))]@(!arities));
				i := (!i)+1
			);
			i := 0;
		while ((!i) < (length (!symbols))) do
			(
				if (valOf(Int.fromString(List.nth(!arities, !i))) < 0) then ((valid := false); (j := length (!symbols)); (i := length (!symbols)))
				else (j := (!i) + 1);

				while ((!j) < (length (!symbols))) do
				(
					if ((List.nth(!symbols, !i) = List.nth(!symbols, !j)) andalso (List.nth(!arities, !i) <> List.nth(!arities, !j))) then ((valid := false); (j := (length (!symbols))); (i := length (!symbols)))
					else (j := (!j)+1);
					j := (!j)+1
					
				);
				i := (!i) + 1
			);
			!valid
	end

(* 
fun wff name h =
(
	let
	val valid = ref true;
fun	help (Absyn.VARIABLE(x)) = (if (searcharity(!symbols,x,ref 0) = 0) then true else (raise invalid))
  	  | help (LEAF(x)) = (if (searcharity(!symbols,x,ref 0) = 0) then true else (raise invalid))
      | help (NODE(x,l)) = (if (searcharity(!symbols,x,ref 0) = (length l)) then ((List.map help l);true ) else (raise invalid ))
	in
		valid := (check_sig name);
		if((!valid) = true)then (help h) else (raise invalid)
	end
)
 *)

fun predeq (Absyn.NUM(x)) (Absyn.NUM(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.REAL(x)) (Absyn.REAL(y)) = if(Real.==(x,y))then(0)else(1)
  | predeq (Absyn.BOOL(x)) (Absyn.BOOL(y)) = if(x=y)then(0)else(1)
  (*| predeq (Absyn.SUBLEFT(x)) (Absyn.SUBLEFT(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.SUBRIGHT(x)) (Absyn.SUBRIGHT(y)) = if(x=y)then(0)else(1)*)
  | predeq (Absyn.STRING(x)) (Absyn.STRING(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.UNARY_OP(x,l1)) (Absyn.UNARY_OP(y,l2)) = if(x=y)then(predeq l1 l2)else(1)
  | predeq (Absyn.BINARY_OP(x,l1,l2)) (Absyn.BINARY_OP(y,l3,l4)) = if(x=y)then((predeq l1 l3)+(predeq l2 l4))else(1)
  | predeq (Absyn.PREDICATE(x,l1)) (Absyn.PREDICATE(y,l2)) =
    let 
      val i = ref 0;
      val ans = ref 0;
    in
       if(x=y)then(
          if((length l1) = (length l2))then(
          while((!i) < (length l1))do
            (ans := (!ans)+(predeq (List.nth (l1,(!i))) (List.nth (l2,(!i)))));
            (!ans)
          )else(1)
       )else(1)
    end
  | predeq (Absyn.LIST(l1)) (Absyn.LIST(l2)) =
    let 
      val i = ref 0;
      val ans = ref 0;
    in
          if((length l1) = (length l2))then(
          while((!i) < (length l1))do
            (ans := (!ans)+(predeq (List.nth (l1,(!i))) (List.nth (l2,(!i)))));
            (!ans)
          )else(1)
    end
  | predeq (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = if(x=y)then(0)else(1)
  | predeq _ _ = 1





fun tablereturn ([]) (Absyn.VARIABLE(x)) = (Absyn.VARIABLE(x))     (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!!!*)
  | tablereturn (((Absyn.VARIABLE(a) , b)::ls)) (Absyn.VARIABLE(x)) = (if (a = x) then (b) else (tablereturn (ls) (Absyn.VARIABLE(x))))
  | tablereturn (_::ls) (Absyn.VARIABLE(x)) = (tablereturn ls (Absyn.VARIABLE(x)))

fun subst s (Absyn.VARIABLE(x)) = (tablereturn s (Absyn.VARIABLE(x))) 
  | subst s (Absyn.BINARY_OP(x,l1,l2)) = (Absyn.BINARY_OP(x,(subst s l1),(subst s l2)))
  | subst s (Absyn.UNARY_OP(x,l)) = (Absyn.UNARY_OP(x,(subst s l)))
  | subst s (Absyn.PREDICATE(x,l)) = (Absyn.PREDICATE(x,(List.map (subst s) l)))
  | subst s (Absyn.LIST(l)) = (Absyn.LIST(List.map (subst s) l))
  | subst s (x) = (x)
  (* | subst s (Absyn.REAL(x)) = (Absyn.REAL(x))
  | subst s (Absyn.BOOL(x)) = (Absyn.BOOL(x))
  | subst s (Absyn.SUBLEFT(x)) = (Absyn.SUBLEFT(x))
  | subst s (Absyn.SUBRIGHT(x)) = (Absyn.SUBRIGHT(x))
  | subst s (Absyn.STRING(x)) = (Absyn.STRING(x))
 *)

fun searchright (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = if (x = y) then (print "eqi searchright\n";1) else (print "not equal searchright\n";0)
  | searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l)) = (foldl (fn(a,r)=>(searchright (Absyn.VARIABLE(x)) a) + r) 0 l)
  | searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l)) = (searchright (Absyn.VARIABLE(x)) l)
  | searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2)) = ((searchright (Absyn.VARIABLE(x)) l1) + (searchright (Absyn.VARIABLE(x)) l2))
  | searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l)) = (foldl (fn(a,r)=>(searchright (Absyn.VARIABLE(x)) a) + r) 0 l)
  | searchright (_) (_) = 0
  (* | searchright (Absyn.VARIABLE(x)) (Absyn.REAL(y)) = 0
  | searchright (Absyn.VARIABLE(x)) (Absyn.BOOL(y)) = 0
  | searchright (Absyn.VARIABLE(x)) (Absyn.SUBLEFT(y)) = 0
  | searchright (Absyn.VARIABLE(x)) (Absyn.SUBRIGHT(y)) = 0
  | searchright (Absyn.VARIABLE(x)) (Absyn.STRING(y)) = 0 *)
  


fun check_failunify([(Absyn.BOOL(false),Absyn.BOOL(false))]) = true
  | check_failunify(_) = false

fun common ([],b,ans1) = ans1
  | common ((x1,x2)::y,b,ans1) = common(y,b,ans1 @ [(x1,(subst b x2))])

fun mgu (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = (if(x=y)then ([])else([(Absyn.VARIABLE(x),Absyn.VARIABLE(y))]))
  | mgu (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l))) <> 0) then (print "Absyn.VARIABLE and predicate\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.PREDICATE(y,l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l))) <> 0) then (print "Absyn.VARIABLE and unary_op\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.UNARY_OP(y,l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2))) <> 0) then (print "Absyn.VARIABLE and binary_op\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.BINARY_OP(y,l1,l2))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  (* | mgu (Absyn.VARIABLE(x)) (Absyn.REAL(y)) = ([(Absyn.VARIABLE(x),Absyn.REAL(y))])
  | mgu (Absyn.VARIABLE(x)) (Absyn.BOOL(y)) = ([(Absyn.VARIABLE(x),Absyn.BOOL(y))])
  | mgu (Absyn.VARIABLE(x)) (Absyn.SUBLEFT(y)) = ([(Absyn.VARIABLE(x),Absyn.SUBLEFT(y))])
  | mgu (Absyn.VARIABLE(x)) (Absyn.SUBRIGHT(y)) = ([(Absyn.VARIABLE(x),Absyn.SUBRIGHT(y))])
  | mgu (Absyn.VARIABLE(x)) (Absyn.STRING(y)) = ([(Absyn.VARIABLE(x),Absyn.STRING(y))])
 *)
  | mgu (Absyn.PREDICATE(y,l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l))) <> 0) then (print "Absyn.VARIABLE and predicate\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.PREDICATE(y,l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  
  | mgu (Absyn.UNARY_OP(y,l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l))) <> 0) then (print "Absyn.VARIABLE and unary_op\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.UNARY_OP(y,l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.BINARY_OP(y,l1,l2)) (Absyn.VARIABLE(x)) =  (if ((searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2))) <> 0) then (print "Absyn.VARIABLE and binary_op\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.BINARY_OP(y,l1,l2))])      (* INSERT EXCEPTION  !!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.LIST(l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l))) <> 0) then (print "Absyn.VARIABLE and list\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.LIST(l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*) 
  | mgu (Absyn.VARIABLE(x)) (Absyn.LIST(l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l))) <> 0) then (print "Absyn.VARIABLE and predicate\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.LIST(l))])      (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!*)
  

  | mgu (Absyn.VARIABLE(x)) (y) = ([(Absyn.VARIABLE(x),y)])
  | mgu (x) (Absyn.VARIABLE(y)) = ([(Absyn.VARIABLE(y),x)])
  | mgu (Absyn.UNARY_OP(x,l1)) (Absyn.UNARY_OP(y,l2)) = (if (x=y) then (mgu l1 l2) else (print "unary_op and unary_op\n";[(Absyn.BOOL(false),Absyn.BOOL(false))]))
  | mgu (Absyn.BINARY_OP(x,l1,l2)) (Absyn.BINARY_OP(y,l3,l4)) = 
  (
    let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref [];
  in
    if (x <> y) then (print "binary_op and binary_op\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else 
    (
        currho := mgu (subst (!rhos) (l1)) (subst (!rhos) (l3));
        if(check_failunify(!currho)) then (rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
        currho := mgu (subst (!rhos) (l2)) (subst (!rhos) (l4));
        if (check_failunify(!currho)) then (rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho)) ));
      (!rhos)
    )
  end
  )
  | mgu (Absyn.PREDICATE(x,l1)) (Absyn.PREDICATE(y,l2)) = 
  (
    let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref [];
  in
    if (x <> y) then (print "PREDICATE and PREDICATE\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]) else 
    (
      if((length l1) <> (length l2))then (print "predicate and predicate length\n";[(Absyn.BOOL(false),Absyn.BOOL(false))])else(
        while( (!i) < (length l1)) do 
        (
          currho := mgu (subst (!rhos) (List.nth(l1,(!i)))) (subst (!rhos) (List.nth(l2,(!i))));
          if (check_failunify(!currho)) then (i := (length l1); rhos := (!currho)) else (
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
          i := (!i)+1)
        );
        (!rhos)
      )
    )
  end
  )
  | mgu (Absyn.NUM(x)) (Absyn.NUM(y)) = (if (x=y) then [] else (print "num and num\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        (* INSERT exception!!!!!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.REAL(x)) (Absyn.REAL(y)) = (if Real.==(x, y) then [] else (print "real and real\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        (* INSERT exception!!!!!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.BOOL(x)) (Absyn.BOOL(y)) = (if (x=y) then [] else (print "BOOL and BOOL\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        (* INSERT exception!!!!!!!!!!!!!!!!!!!!*)
  (*| mgu (Absyn.SUBLEFT(x)) (Absyn.SUBLEFT(y)) = (if (x=y) then [] else (print "SUBLEFT and SUBLEFT\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        


  | mgu (Absyn.SUBRIGHT(x)) (Absyn.SUBRIGHT(y)) = (if (x=y) then [] else (print "SUBRIGHT and SUBRIGHT\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        *)
  | mgu (Absyn.STRING(x)) (Absyn.STRING(y)) = (if (x=y) then [] else (print "STRING and STRING\n" ;[(Absyn.BOOL(false),Absyn.BOOL(false))]))        (*INSERT exception!!!!!!!!!!!!!!!!!!!!*)
  | mgu (Absyn.LIST([])) (Absyn.LIST([])) = []
  | mgu (Absyn.LIST([])) (Absyn.LIST(x::ls)) = (print "list and empty\n";[(Absyn.BOOL false,Absyn.BOOL false)])
  | mgu (Absyn.LIST(Absyn.VARIABLE(x)::l1)) (Absyn.LIST(l2)) = 
    let
      val i = ref 0;
      val global = ref [];
      val sub = ref [];
      val currho = ref [];
      val cur_sub = ref ([(Absyn.VARIABLE(x),Absyn.LIST([]))])
      val dupl2 = ref l2;
    in
      currho := mgu (subst (!cur_sub) (Absyn.LIST(l1))) (subst (!cur_sub) (Absyn.LIST(l2)));
        if(check_failunify(!currho))then ()else(global := ((!global)@(!cur_sub)@(!currho)));
      while((!i) < (length l2))do(
        sub := (!sub)@[Stack.top(!dupl2)];
        dupl2 := (Stack.pop(!dupl2));
        cur_sub := [(Absyn.VARIABLE(x),Absyn.LIST(!sub))];
        currho := mgu (subst (!cur_sub) (Absyn.LIST(l1))) (subst (!cur_sub) (Absyn.LIST(!dupl2)));
      
        if(check_failunify(!currho))then (print "entered\n";i := (!i)+1)else(global := ((!global)@(!cur_sub)@(!currho)); i := (!i)+1)
      );
      (!global)
    end
  | mgu (Absyn.LIST(l1)) (Absyn.LIST(Absyn.VARIABLE(x)::l2)) = mgu (Absyn.LIST(Absyn.VARIABLE(x)::l2)) (Absyn.LIST(l1))
  | mgu (Absyn.LIST(x::l1)) (Absyn.LIST(y::l2)) = (if ((predeq x y)=0)then (mgu (Absyn.LIST(l1)) (Absyn.LIST(l2))) else(print "list and list\n";[(Absyn.BOOL(false),Absyn.BOOL(false))]))

  | mgu (Absyn.LIST(x)) (Absyn.LIST(y)) = mgu (Absyn.LIST(y)) (Absyn.LIST(x))
  | mgu _ _ = ([(Absyn.BOOL false, Absyn.BOOL false)])
 

fun mgulist termlist = 
  let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref[];

  in
    if ((length termlist) < 2) then [] else(
      while((!i) < ((length termlist)-1)) do
      (
        currho := mgu (subst (!rhos) (List.nth(termlist,(!i)))) (subst (!rhos) (List.nth(termlist,(!i)+1)));
        if(check_failunify(!currho)) then ((i) := ((length termlist));print "failed mgulist\n"; rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho));
        i := (!i)+1)
      );

      (!rhos)
    )
  end

