edge(1,2).
edge(2,3).
edge(3,4).
edge(5,6).

path(X,X).
path(X,Y) :- edge(X,Z),path(Z,Y).
?- path(1,3).
;