structure Stack = 
struct
exception emptyStack;
val new = [];
fun isempty x = (x = []);
fun push x s = x::s;
fun pop (x::s) = s
  | pop [] = raise emptyStack;
fun top (x::s) = x
  | top [] = raise emptyStack;
end

structure Absyn =
   struct
      datatype program = PROG of declare list
                        (* | PROGL of declare *)
                        | NONE
        and declare = DEC_FACT of predicate
                     | DEC_RULE of predicate * predicate list
                     | GOAL of predicate list
        and predicate = PREDICATE of string * predicate list
                      | NUM of int
                      | REAL of real
                      | BOOL of bool
                      | STRING of string
                      | VARIABLE of string
                      | UNARY_OP of string * predicate
                      | BINARY_OP of string*predicate*predicate
                      | LIST of predicate list
                      | LISTBAR of predicate * predicate
        
end




