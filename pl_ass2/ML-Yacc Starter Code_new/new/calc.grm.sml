functor CalcLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Calc_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
val fact_rules : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goals : (Absyn.predicate list) list ref = ref [];
val fr_list : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goal_list : (Absyn.predicate list) list ref = ref [];

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\006\000\044\000\008\000\043\000\009\000\042\000\
\\010\000\041\000\011\000\040\000\012\000\039\000\029\000\038\000\
\\031\000\037\000\032\000\036\000\033\000\035\000\040\000\084\000\000\000\
\\001\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\010\000\041\000\011\000\040\000\012\000\039\000\
\\020\000\122\000\026\000\122\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\040\000\122\000\041\000\122\000\000\000\
\\001\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\010\000\041\000\011\000\040\000\012\000\039\000\
\\020\000\123\000\026\000\123\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\040\000\123\000\041\000\123\000\000\000\
\\001\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\010\000\041\000\011\000\040\000\012\000\039\000\
\\020\000\124\000\026\000\124\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\040\000\124\000\041\000\124\000\000\000\
\\001\000\013\000\030\000\014\000\029\000\015\000\028\000\016\000\012\000\
\\017\000\027\000\018\000\026\000\027\000\025\000\030\000\024\000\
\\039\000\023\000\000\000\
\\001\000\013\000\030\000\014\000\029\000\015\000\028\000\017\000\027\000\
\\018\000\026\000\027\000\025\000\030\000\024\000\039\000\023\000\000\000\
\\001\000\013\000\030\000\014\000\029\000\015\000\028\000\017\000\027\000\
\\018\000\026\000\027\000\025\000\030\000\024\000\039\000\081\000\000\000\
\\001\000\016\000\012\000\000\000\
\\001\000\019\000\013\000\000\000\
\\001\000\019\000\051\000\000\000\
\\001\000\020\000\052\000\000\000\
\\001\000\020\000\079\000\000\000\
\\001\000\021\000\014\000\000\000\
\\001\000\034\000\000\000\037\000\000\000\000\000\
\\001\000\035\000\033\000\000\000\
\\001\000\035\000\057\000\000\000\
\\001\000\040\000\076\000\000\000\
\\001\000\040\000\086\000\000\000\
\\089\000\000\000\
\\090\000\016\000\012\000\038\000\011\000\000\000\
\\091\000\016\000\012\000\038\000\011\000\000\000\
\\092\000\000\000\
\\093\000\000\000\
\\094\000\000\000\
\\095\000\000\000\
\\096\000\000\000\
\\097\000\000\000\
\\098\000\026\000\050\000\000\000\
\\099\000\000\000\
\\100\000\000\000\
\\101\000\035\000\015\000\000\000\
\\102\000\000\000\
\\103\000\000\000\
\\104\000\026\000\034\000\000\000\
\\105\000\000\000\
\\106\000\000\000\
\\107\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\006\000\044\000\008\000\043\000\009\000\042\000\
\\010\000\041\000\011\000\040\000\012\000\039\000\029\000\038\000\
\\031\000\037\000\032\000\036\000\033\000\035\000\000\000\
\\108\000\000\000\
\\109\000\000\000\
\\110\000\000\000\
\\111\000\000\000\
\\112\000\000\000\
\\113\000\000\000\
\\114\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\115\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\116\000\003\000\047\000\004\000\046\000\005\000\045\000\029\000\038\000\
\\031\000\037\000\000\000\
\\117\000\003\000\047\000\004\000\046\000\005\000\045\000\029\000\038\000\
\\031\000\037\000\000\000\
\\118\000\031\000\037\000\000\000\
\\119\000\031\000\037\000\000\000\
\\120\000\031\000\037\000\000\000\
\\121\000\003\000\047\000\004\000\046\000\005\000\045\000\031\000\037\000\000\000\
\\125\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\126\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\127\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\128\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\006\000\044\000\008\000\043\000\009\000\042\000\
\\010\000\041\000\011\000\040\000\012\000\039\000\029\000\038\000\
\\031\000\037\000\032\000\036\000\033\000\035\000\000\000\
\\129\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\130\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\012\000\039\000\029\000\038\000\031\000\037\000\
\\032\000\036\000\033\000\035\000\000\000\
\\131\000\000\000\
\\131\000\040\000\087\000\000\000\
\\132\000\000\000\
\\133\000\000\000\
\\134\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\006\000\044\000\008\000\043\000\009\000\042\000\
\\010\000\041\000\011\000\040\000\012\000\039\000\026\000\078\000\
\\029\000\038\000\031\000\037\000\032\000\036\000\033\000\035\000\000\000\
\\134\000\001\000\049\000\002\000\048\000\003\000\047\000\004\000\046\000\
\\005\000\045\000\006\000\044\000\008\000\043\000\009\000\042\000\
\\010\000\041\000\011\000\040\000\012\000\039\000\026\000\078\000\
\\029\000\038\000\031\000\037\000\032\000\036\000\033\000\035\000\
\\041\000\077\000\000\000\
\\135\000\000\000\
\\136\000\013\000\030\000\014\000\029\000\015\000\028\000\017\000\027\000\
\\018\000\026\000\027\000\025\000\030\000\024\000\039\000\023\000\000\000\
\"
val actionRowNumbers =
"\019\000\008\000\024\000\012\000\
\\023\000\030\000\022\000\018\000\
\\020\000\007\000\035\000\004\000\
\\007\000\025\000\021\000\014\000\
\\033\000\036\000\027\000\009\000\
\\010\000\064\000\005\000\005\000\
\\038\000\042\000\041\000\039\000\
\\040\000\031\000\015\000\032\000\
\\007\000\005\000\005\000\005\000\
\\005\000\005\000\005\000\005\000\
\\005\000\005\000\005\000\005\000\
\\005\000\005\000\005\000\005\000\
\\004\000\004\000\026\000\016\000\
\\062\000\044\000\043\000\029\000\
\\034\000\056\000\055\000\054\000\
\\050\000\053\000\052\000\051\000\
\\003\000\002\000\001\000\049\000\
\\048\000\047\000\046\000\045\000\
\\028\000\011\000\057\000\006\000\
\\064\000\037\000\000\000\064\000\
\\063\000\061\000\060\000\017\000\
\\058\000\059\000\013\000"
val gotoT =
"\
\\001\000\086\000\002\000\008\000\003\000\007\000\004\000\006\000\
\\005\000\005\000\007\000\004\000\008\000\003\000\010\000\002\000\
\\012\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\008\000\003\000\014\000\004\000\006\000\005\000\005\000\
\\007\000\004\000\008\000\003\000\010\000\002\000\012\000\001\000\000\000\
\\005\000\016\000\011\000\015\000\012\000\001\000\000\000\
\\000\000\
\\006\000\020\000\012\000\019\000\013\000\018\000\014\000\017\000\000\000\
\\005\000\016\000\009\000\030\000\011\000\029\000\012\000\001\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\014\000\052\000\015\000\051\000\000\000\
\\014\000\053\000\000\000\
\\014\000\054\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\005\000\016\000\011\000\056\000\012\000\001\000\000\000\
\\014\000\057\000\000\000\
\\014\000\058\000\000\000\
\\014\000\059\000\000\000\
\\014\000\060\000\000\000\
\\014\000\061\000\000\000\
\\014\000\062\000\000\000\
\\014\000\063\000\000\000\
\\014\000\064\000\000\000\
\\014\000\065\000\000\000\
\\014\000\066\000\000\000\
\\014\000\067\000\000\000\
\\014\000\068\000\000\000\
\\014\000\069\000\000\000\
\\014\000\070\000\000\000\
\\014\000\071\000\000\000\
\\006\000\072\000\012\000\019\000\013\000\018\000\014\000\017\000\000\000\
\\006\000\073\000\012\000\019\000\013\000\018\000\014\000\017\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\014\000\078\000\000\000\
\\014\000\081\000\015\000\080\000\000\000\
\\000\000\
\\000\000\
\\014\000\052\000\015\000\083\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 87
val numrules = 48
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | NUM of unit ->  (int) | CAPID of unit ->  (string)
 | SMALLID of unit ->  (string) | STRING of unit ->  (string)
 | REAL of unit ->  (real) | BOOL of unit ->  (bool)
 | expression_seq of unit ->  (Absyn.predicate list)
 | expression of unit ->  (Absyn.predicate)
 | term of unit ->  (Absyn.predicate)
 | predicatesymbol of unit ->  (string)
 | predicatesequence of unit ->  (Absyn.predicate list)
 | goal of unit ->  (Absyn.predicate list)
 | body of unit ->  (Absyn.predicate list)
 | head of unit ->  (Absyn.predicate)
 | rule of unit ->  (Absyn.predicate*Absyn.predicate list)
 | termsequence of unit ->  (Absyn.predicate list)
 | predicate of unit ->  (Absyn.predicate)
 | fact of unit ->  (Absyn.predicate)
 | declaration_seq of unit ->  ( ( Absyn.predicate * Absyn.predicate list )  list)
 | declaration of unit ->  ( ( Absyn.predicate * Absyn.predicate list ) )
 | program of unit ->  ( ( Absyn.predicate * Absyn.predicate )  list)
end
type svalue = MlyValue.svalue
type result =  ( Absyn.predicate * Absyn.predicate )  list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 35) => true | (T 36) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
 $$ (T 15),nil
 $$ (T 35))::
(nil
 $$ (T 16),nil
 $$ (T 35))::
nil
val noShift = 
fn (T 33) => true | _ => false
val showTerminal =
fn (T 0) => "PLUS"
  | (T 1) => "SUB"
  | (T 2) => "TIMES"
  | (T 3) => "INTDIV"
  | (T 4) => "REALDIV"
  | (T 5) => "GREAT"
  | (T 6) => "LESS"
  | (T 7) => "GREATEQ"
  | (T 8) => "LESSEQ"
  | (T 9) => "EQ"
  | (T 10) => "NEQ"
  | (T 11) => "CAT"
  | (T 12) => "BOOL"
  | (T 13) => "REAL"
  | (T 14) => "STRING"
  | (T 15) => "SMALLID"
  | (T 16) => "CAPID"
  | (T 17) => "NUM"
  | (T 18) => "LPAREN"
  | (T 19) => "RPAREN"
  | (T 20) => "IF"
  | (T 21) => "THEN"
  | (T 22) => "ELSE"
  | (T 23) => "LET"
  | (T 24) => "END"
  | (T 25) => "COMMA"
  | (T 26) => "NEGATION"
  | (T 27) => "COLON"
  | (T 28) => "MOD"
  | (T 29) => "ABS"
  | (T 30) => "EXPONENT"
  | (T 31) => "CONJUNCT"
  | (T 32) => "DISJUNCT"
  | (T 33) => "EOF"
  | (T 34) => "DOT"
  | (T 35) => "PRINT"
  | (T 36) => "SEMI"
  | (T 37) => "QUES"
  | (T 38) => "LSBRACKET"
  | (T 39) => "RSBRACKET"
  | (T 40) => "LISTBAR"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 40) $$ (T 39) $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34)
 $$ (T 33) $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27)
 $$ (T 26) $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20)
 $$ (T 19) $$ (T 18) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7)
 $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 2) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.declaration_seq declaration_seq1, 
declaration_seq1left, declaration_seq1right)) :: rest671)) => let val 
 result = MlyValue.program (fn _ => let val  declaration_seq1 = 
declaration_seq1 ()
 in (
fact_rules := (List.rev(!fr_list));goals := (List.rev(!goal_list));fr_list := [];goal_list := [];((!(fact_rules)),(!(goals))); (Unify.finalcheck(!fact_rules,!goals,Unify.main(!fact_rules,!goals,[])))
)
end)
 in ( LrTable.NT 0, ( result, declaration_seq1left, 
declaration_seq1right), rest671)
end
|  ( 1, ( rest671)) => let val  result = MlyValue.program (fn _ => (
([])))
 in ( LrTable.NT 0, ( result, defaultPos, defaultPos), rest671)
end
|  ( 2, ( ( _, ( MlyValue.declaration declaration1, declaration1left, 
declaration1right)) :: rest671)) => let val  result = 
MlyValue.declaration_seq (fn _ => let val  (declaration as 
declaration1) = declaration1 ()
 in ([declaration])
end)
 in ( LrTable.NT 2, ( result, declaration1left, declaration1right), 
rest671)
end
|  ( 3, ( ( _, ( MlyValue.declaration_seq declaration_seq1, _, 
declaration_seq1right)) :: ( _, ( MlyValue.declaration declaration1, 
declaration1left, _)) :: rest671)) => let val  result = 
MlyValue.declaration_seq (fn _ => let val  (declaration as 
declaration1) = declaration1 ()
 val  (declaration_seq as declaration_seq1) = declaration_seq1 ()
 in (declaration::declaration_seq)
end)
 in ( LrTable.NT 2, ( result, declaration1left, declaration_seq1right)
, rest671)
end
|  ( 4, ( ( _, ( MlyValue.fact fact1, fact1left, fact1right)) :: 
rest671)) => let val  result = MlyValue.declaration (fn _ => let val 
 (fact as fact1) = fact1 ()
 in (fr_list := ((fact,[])::(!fr_list));(fact,[]))
end)
 in ( LrTable.NT 1, ( result, fact1left, fact1right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.rule rule1, rule1left, rule1right)) :: 
rest671)) => let val  result = MlyValue.declaration (fn _ => let val 
 (rule as rule1) = rule1 ()
 in (fr_list := (rule::(!fr_list));rule)
end)
 in ( LrTable.NT 1, ( result, rule1left, rule1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.goal goal1, goal1left, goal1right)) :: 
rest671)) => let val  result = MlyValue.declaration (fn _ => let val 
 (goal as goal1) = goal1 ()
 in (goal_list := (!goal_list)@[goal];(Absyn.STRING "goal",goal))
end)
 in ( LrTable.NT 1, ( result, goal1left, goal1right), rest671)
end
|  ( 7, ( ( _, ( _, _, DOT1right)) :: ( _, ( MlyValue.predicate 
predicate1, predicate1left, _)) :: rest671)) => let val  result = 
MlyValue.fact (fn _ => let val  (predicate as predicate1) = predicate1
 ()
 in (predicate)
end)
 in ( LrTable.NT 3, ( result, predicate1left, DOT1right), rest671)
end
|  ( 8, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.termsequence 
termsequence1, _, _)) :: _ :: ( _, ( MlyValue.predicatesymbol 
predicatesymbol1, predicatesymbol1left, _)) :: rest671)) => let val  
result = MlyValue.predicate (fn _ => let val  (predicatesymbol as 
predicatesymbol1) = predicatesymbol1 ()
 val  (termsequence as termsequence1) = termsequence1 ()
 in (Absyn.PREDICATE (predicatesymbol,termsequence))
end)
 in ( LrTable.NT 4, ( result, predicatesymbol1left, RPAREN1right), 
rest671)
end
|  ( 9, ( ( _, ( MlyValue.term term1, term1left, term1right)) :: 
rest671)) => let val  result = MlyValue.termsequence (fn _ => let val 
 (term as term1) = term1 ()
 in ([term])
end)
 in ( LrTable.NT 5, ( result, term1left, term1right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.termsequence termsequence1, _, 
termsequence1right)) :: _ :: ( _, ( MlyValue.term term1, term1left, _)
) :: rest671)) => let val  result = MlyValue.termsequence (fn _ => let
 val  (term as term1) = term1 ()
 val  (termsequence as termsequence1) = termsequence1 ()
 in (term::termsequence)
end)
 in ( LrTable.NT 5, ( result, term1left, termsequence1right), rest671)

end
|  ( 11, ( ( _, ( _, _, DOT1right)) :: ( _, ( MlyValue.body body1, _,
 _)) :: _ :: ( _, ( MlyValue.head head1, head1left, _)) :: rest671))
 => let val  result = MlyValue.rule (fn _ => let val  (head as head1)
 = head1 ()
 val  (body as body1) = body1 ()
 in (head,body)
end)
 in ( LrTable.NT 6, ( result, head1left, DOT1right), rest671)
end
|  ( 12, ( ( _, ( MlyValue.predicate predicate1, predicate1left, 
predicate1right)) :: rest671)) => let val  result = MlyValue.head (fn
 _ => let val  (predicate as predicate1) = predicate1 ()
 in (predicate)
end)
 in ( LrTable.NT 7, ( result, predicate1left, predicate1right), 
rest671)
end
|  ( 13, ( ( _, ( MlyValue.predicatesequence predicatesequence1, 
predicatesequence1left, predicatesequence1right)) :: rest671)) => let
 val  result = MlyValue.body (fn _ => let val  (predicatesequence as 
predicatesequence1) = predicatesequence1 ()
 in (predicatesequence)
end)
 in ( LrTable.NT 8, ( result, predicatesequence1left, 
predicatesequence1right), rest671)
end
|  ( 14, ( ( _, ( _, _, DOT1right)) :: ( _, ( 
MlyValue.predicatesequence predicatesequence1, _, _)) :: ( _, ( _, 
QUES1left, _)) :: rest671)) => let val  result = MlyValue.goal (fn _
 => let val  (predicatesequence as predicatesequence1) = 
predicatesequence1 ()
 in (predicatesequence)
end)
 in ( LrTable.NT 9, ( result, QUES1left, DOT1right), rest671)
end
|  ( 15, ( ( _, ( MlyValue.predicate predicate1, predicate1left, 
predicate1right)) :: rest671)) => let val  result = 
MlyValue.predicatesequence (fn _ => let val  (predicate as predicate1)
 = predicate1 ()
 in ([predicate])
end)
 in ( LrTable.NT 10, ( result, predicate1left, predicate1right), 
rest671)
end
|  ( 16, ( ( _, ( MlyValue.predicatesequence predicatesequence1, _, 
predicatesequence1right)) :: _ :: ( _, ( MlyValue.predicate predicate1
, predicate1left, _)) :: rest671)) => let val  result = 
MlyValue.predicatesequence (fn _ => let val  (predicate as predicate1)
 = predicate1 ()
 val  (predicatesequence as predicatesequence1) = predicatesequence1
 ()
 in (predicate::predicatesequence)
end)
 in ( LrTable.NT 10, ( result, predicate1left, predicatesequence1right
), rest671)
end
|  ( 17, ( ( _, ( MlyValue.SMALLID SMALLID1, SMALLID1left, 
SMALLID1right)) :: rest671)) => let val  result = 
MlyValue.predicatesymbol (fn _ => let val  (SMALLID as SMALLID1) = 
SMALLID1 ()
 in (SMALLID)
end)
 in ( LrTable.NT 11, ( result, SMALLID1left, SMALLID1right), rest671)

end
|  ( 18, ( ( _, ( MlyValue.expression expression1, expression1left, 
expression1right)) :: rest671)) => let val  result = MlyValue.term (fn
 _ => let val  (expression as expression1) = expression1 ()
 in (expression)
end)
 in ( LrTable.NT 12, ( result, expression1left, expression1right), 
rest671)
end
|  ( 19, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.termsequence
 termsequence1, _, _)) :: _ :: ( _, ( MlyValue.predicatesymbol 
predicatesymbol1, predicatesymbol1left, _)) :: rest671)) => let val  
result = MlyValue.term (fn _ => let val  (predicatesymbol as 
predicatesymbol1) = predicatesymbol1 ()
 val  (termsequence as termsequence1) = termsequence1 ()
 in (Absyn.PREDICATE (predicatesymbol,termsequence))
end)
 in ( LrTable.NT 12, ( result, predicatesymbol1left, RPAREN1right), 
rest671)
end
|  ( 20, ( ( _, ( MlyValue.NUM NUM1, NUM1left, NUM1right)) :: rest671)
) => let val  result = MlyValue.expression (fn _ => let val  (NUM as 
NUM1) = NUM1 ()
 in (Absyn.NUM NUM)
end)
 in ( LrTable.NT 13, ( result, NUM1left, NUM1right), rest671)
end
|  ( 21, ( ( _, ( MlyValue.REAL REAL1, REAL1left, REAL1right)) :: 
rest671)) => let val  result = MlyValue.expression (fn _ => let val  (
REAL as REAL1) = REAL1 ()
 in (Absyn.REAL REAL)
end)
 in ( LrTable.NT 13, ( result, REAL1left, REAL1right), rest671)
end
|  ( 22, ( ( _, ( MlyValue.BOOL BOOL1, BOOL1left, BOOL1right)) :: 
rest671)) => let val  result = MlyValue.expression (fn _ => let val  (
BOOL as BOOL1) = BOOL1 ()
 in (Absyn.BOOL BOOL)
end)
 in ( LrTable.NT 13, ( result, BOOL1left, BOOL1right), rest671)
end
|  ( 23, ( ( _, ( MlyValue.STRING STRING1, STRING1left, STRING1right))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  (STRING as STRING1) = STRING1 ()
 in (Absyn.STRING STRING)
end)
 in ( LrTable.NT 13, ( result, STRING1left, STRING1right), rest671)

end
|  ( 24, ( ( _, ( MlyValue.CAPID CAPID1, CAPID1left, CAPID1right)) :: 
rest671)) => let val  result = MlyValue.expression (fn _ => let val  (
CAPID as CAPID1) = CAPID1 ()
 in (Absyn.VARIABLE CAPID)
end)
 in ( LrTable.NT 13, ( result, CAPID1left, CAPID1right), rest671)
end
|  ( 25, ( ( _, ( MlyValue.expression expression1, _, expression1right
)) :: ( _, ( _, NEGATION1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (expression as expression1) = 
expression1 ()
 in (Absyn.UNARY_OP ("NEGATION",expression))
end)
 in ( LrTable.NT 13, ( result, NEGATION1left, expression1right), 
rest671)
end
|  ( 26, ( ( _, ( MlyValue.expression expression1, _, expression1right
)) :: ( _, ( _, ABS1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (expression as expression1) = 
expression1 ()
 in (Absyn.UNARY_OP ("ABS",expression))
end)
 in ( LrTable.NT 13, ( result, ABS1left, expression1right), rest671)

end
|  ( 27, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("PLUS",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 28, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("SUB",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 29, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("TIMES",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 30, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("INTDIV",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 31, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("REALDIV",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 32, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("MOD",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 33, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("GREAT",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 34, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("GREATEQ",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 35, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("LESSEQ",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 36, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("EQ",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 37, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("NEQ",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 38, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("CAT",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 39, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("EXPONENT",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 40, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("CONJUNCT",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 41, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.BINARY_OP ("DISJUNCT",expression1,expression2))
end)
 in ( LrTable.NT 13, ( result, expression1left, expression2right), 
rest671)
end
|  ( 42, ( ( _, ( _, _, RSBRACKET1right)) :: ( _, ( 
MlyValue.expression_seq expression_seq1, _, _)) :: ( _, ( _, 
LSBRACKET1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (expression_seq as 
expression_seq1) = expression_seq1 ()
 in (Absyn.LIST (expression_seq))
end)
 in ( LrTable.NT 13, ( result, LSBRACKET1left, RSBRACKET1right), 
rest671)
end
|  ( 43, ( ( _, ( _, _, RSBRACKET2right)) :: _ :: ( _, ( 
MlyValue.expression_seq expression_seq1, _, _)) :: _ :: _ :: ( _, ( 
MlyValue.expression expression1, _, _)) :: ( _, ( _, LSBRACKET1left, _
)) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  (expression as expression1) = expression1 ()
 val  (expression_seq as expression_seq1) = expression_seq1 ()
 in (Absyn.LIST (expression::(expression_seq)))
end)
 in ( LrTable.NT 13, ( result, LSBRACKET1left, RSBRACKET2right), 
rest671)
end
|  ( 44, ( ( _, ( _, _, RSBRACKET1right)) :: ( _, ( 
MlyValue.expression expression2, _, _)) :: _ :: ( _, ( 
MlyValue.expression expression1, _, _)) :: ( _, ( _, LSBRACKET1left, _
)) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.LISTBAR(expression1,(expression2)))
end)
 in ( LrTable.NT 13, ( result, LSBRACKET1left, RSBRACKET1right), 
rest671)
end
|  ( 45, ( ( _, ( MlyValue.expression expression1, expression1left, 
expression1right)) :: rest671)) => let val  result = 
MlyValue.expression_seq (fn _ => let val  (expression as expression1)
 = expression1 ()
 in ([expression])
end)
 in ( LrTable.NT 14, ( result, expression1left, expression1right), 
rest671)
end
|  ( 46, ( ( _, ( MlyValue.expression_seq expression_seq1, _, 
expression_seq1right)) :: _ :: ( _, ( MlyValue.expression expression1,
 expression1left, _)) :: rest671)) => let val  result = 
MlyValue.expression_seq (fn _ => let val  (expression as expression1)
 = expression1 ()
 val  (expression_seq as expression_seq1) = expression_seq1 ()
 in (expression::expression_seq)
end)
 in ( LrTable.NT 14, ( result, expression1left, expression_seq1right),
 rest671)
end
|  ( 47, ( rest671)) => let val  result = MlyValue.expression_seq (fn
 _ => ([]))
 in ( LrTable.NT 14, ( result, defaultPos, defaultPos), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Calc_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun SUB (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun INTDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun REALDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun GREAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun LESS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun GREATEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LESSEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun CAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun BOOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.BOOL (fn () => i),p1,p2))
fun REAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.REAL (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun SMALLID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.SMALLID (fn () => i),p1,p2))
fun CAPID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.CAPID (fn () => i),p1,p2))
fun NUM (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.NUM (fn () => i),p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun NEGATION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun MOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun ABS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun EXPONENT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun CONJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun DISJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun DOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun PRINT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun QUES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun LSBRACKET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun RSBRACKET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.VOID,p1,p2))
fun LISTBAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(
ParserData.MlyValue.VOID,p1,p2))
end
end
