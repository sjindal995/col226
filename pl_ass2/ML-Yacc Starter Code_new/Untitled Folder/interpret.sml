structure Unify = 
  struct
    exception invalid
    exception failUnify
    exception invalidConversion
    exception invalid_tablereturn
    exception invalid_returnall
    
    val index = ref 0;

fun printpred (Absyn.LIST(x)) =
		let
			val i = ref 0;
			val str = ref ("[")
		in
			while((!i)<((length x)-1))do(
				str := (!str)^(printpred (List.nth(x,(!i))))^",";
				i := (!i)+1
			);
			str := (!str)^(printpred (List.nth(x,(!i))))^"]";
			(!str)
		end
	| printpred (Absyn.LISTBAR(x,y)) = ("["^(printpred x)^","^(printpred y)^")")
	| printpred (Absyn.PREDICATE(x,y)) =
		let 
			val i = ref 0;
			val str = ref (x^"(");
		in
			while((!i)<((length y)-1))do(
				str := (!str)^(printpred (List.nth(y,(!i))))^",";
				i := (!i)+1
			);
			str := (!str)^(printpred (List.nth(y,(!i))))^(")");
			(!str)
		end

	| printpred (Absyn.NUM x) = ((Int.toString(x)))
	| printpred (Absyn.REAL x) = ((Real.toString(x)))
	| printpred (Absyn.BOOL x) = ((Bool.toString(x)))
	| printpred (Absyn.STRING x) = (x)
	| printpred (Absyn.VARIABLE x) = (x)
	| printpred (Absyn.UNARY_OP(x,l)) = ((x)^"("^(printpred l)^")")
	| printpred (Absyn.BINARY_OP(x,l1,l2)) = ((x)^"("^(printpred l1)^","^(printpred l2)^")")

fun tablereturn ([]) (Absyn.VARIABLE(x)) = (Absyn.VARIABLE(x))     (* INSERT EXCEPTION !!!!!!!!!!!!!!!!!!!*)
  | tablereturn (((Absyn.VARIABLE(a) , b)::ls)) (Absyn.VARIABLE(x)) = (if (a = x) then (b) else (tablereturn (ls) (Absyn.VARIABLE(x))))
  | tablereturn (_::ls) (Absyn.VARIABLE(x)) = (tablereturn ls (Absyn.VARIABLE(x)))
  | tablereturn a b = raise invalid_tablereturn


fun listbar2list (Absyn.LISTBAR(x,Absyn.LIST(y))) = (Absyn.LIST(x::y))
  | listbar2list (Absyn.LISTBAR(x,Absyn.VARIABLE(y))) = (Absyn.LISTBAR(x,Absyn.VARIABLE(y)))
  | listbar2list (Absyn.LISTBAR(x,Absyn.LISTBAR(y,l))) = (Absyn.LISTBAR(x,Absyn.LISTBAR(y,l)))
  | listbar2list x = ((*print (printpred x);*) raise invalidConversion)

fun subst s (Absyn.VARIABLE(x)) = (tablereturn s (Absyn.VARIABLE(x))) 
  | subst s (Absyn.BINARY_OP(x,l1,l2)) = (Absyn.BINARY_OP(x,(subst s l1),(subst s l2)))
  | subst s (Absyn.UNARY_OP(x,l)) = (Absyn.UNARY_OP(x,(subst s l)))
  | subst s (Absyn.PREDICATE(x,l)) = (Absyn.PREDICATE(x,(List.map (subst s) l)))
  | subst s (Absyn.LISTBAR(x,y)) = listbar2list(Absyn.LISTBAR(subst s x,(subst s y)))
  | subst s (Absyn.LIST(l)) = (Absyn.LIST(List.map (subst s) l))
  | subst s (x) = (x)
  


fun searchright (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = if (x = y) then ((*print "eqi searchright\n";*)1) else ((*print "not equal searchright\n";*)0)
  | searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l)) = (foldl (fn(a,r)=>(searchright (Absyn.VARIABLE(x)) a) + r) 0 l)
  | searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l)) = (searchright (Absyn.VARIABLE(x)) l)
  | searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2)) = ((searchright (Absyn.VARIABLE(x)) l1) + (searchright (Absyn.VARIABLE(x)) l2))
  | searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l)) = (foldl (fn(a,r)=>(searchright (Absyn.VARIABLE(x)) a) + r) 0 l)
  | searchright (Absyn.VARIABLE(x)) (Absyn.LISTBAR(y,z)) = ((searchright (Absyn.VARIABLE(x)) y) + (searchright (Absyn.VARIABLE(x)) z))
  | searchright (_) (_) = 0
  

fun check_failunify([(Absyn.BOOL(false),Absyn.BOOL(false))]) = true
  | check_failunify(_) = false

fun common ([],b,ans1) = ans1
  | common ((x1,x2)::y,b,ans1) = common(y,b,ans1 @ [(x1,(subst b x2))])

fun predeq (Absyn.NUM(x)) (Absyn.NUM(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.REAL(x)) (Absyn.REAL(y)) = if(Real.==(x,y))then(0)else(1)
  | predeq (Absyn.BOOL(x)) (Absyn.BOOL(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.STRING(x)) (Absyn.STRING(y)) = if(x=y)then(0)else(1)
  | predeq (Absyn.UNARY_OP(x,l1)) (Absyn.UNARY_OP(y,l2)) = if(x=y)then(predeq l1 l2)else(1)
  | predeq (Absyn.BINARY_OP(x,l1,l2)) (Absyn.BINARY_OP(y,l3,l4)) = if(x=y)then((predeq l1 l3)+(predeq l2 l4))else(1)
  | predeq (Absyn.PREDICATE(x,l1)) (Absyn.PREDICATE(y,l2)) =
    let 
      val i = ref 0;
      val ans = ref 0;
    in
       if(x=y)then(
          if((length l1) = (length l2))then(
          while((!i) < (length l1))do
            (ans := (!ans)+(predeq (List.nth (l1,(!i))) (List.nth (l2,(!i)))));
            (!ans)
          )else(1)
       )else(1)
    end
  | predeq (Absyn.LIST(l1)) (Absyn.LIST(l2)) = predeq (Absyn.PREDICATE("0",l1)) (Absyn.PREDICATE("0",l2))
  | predeq (Absyn.LISTBAR(x,z1)) (Absyn.LISTBAR(y,z2)) = ((predeq x y)+(predeq z1 z2))
  | predeq (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = if(x=y)then(0)else(1)
  | predeq _ _ = 1





fun mgu (Absyn.VARIABLE(x)) (Absyn.VARIABLE(y)) = (if(x=y)then ([])else([(Absyn.VARIABLE(x),Absyn.VARIABLE(y))]))
  | mgu (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l))) <> 0) then ((*print "Absyn.VARIABLE and predicate\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.PREDICATE(y,l))])      
  | mgu (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l))) <> 0) then ((*print "Absyn.VARIABLE and unary_op\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.UNARY_OP(y,l))])      
  | mgu (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2))) <> 0) then ((*print "Absyn.VARIABLE and binary_op\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.BINARY_OP(y,l1,l2))])      
  
  | mgu (Absyn.PREDICATE(y,l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.PREDICATE(y,l))) <> 0) then ((*print "Absyn.VARIABLE and predicate\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.PREDICATE(y,l))])      
  
  | mgu (Absyn.UNARY_OP(y,l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.UNARY_OP(y,l))) <> 0) then ((*print "Absyn.VARIABLE and unary_op\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.UNARY_OP(y,l))])      
  | mgu (Absyn.BINARY_OP(y,l1,l2)) (Absyn.VARIABLE(x)) =  (if ((searchright (Absyn.VARIABLE(x)) (Absyn.BINARY_OP(y,l1,l2))) <> 0) then ((*print "Absyn.VARIABLE and binary_op\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.BINARY_OP(y,l1,l2))])      
  | mgu (Absyn.LIST(l)) (Absyn.VARIABLE(x)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l))) <> 0) then ((*print "Absyn.VARIABLE and list\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.LIST(l))])      
  | mgu (Absyn.VARIABLE(x)) (Absyn.LIST(l)) = (if ((searchright (Absyn.VARIABLE(x)) (Absyn.LIST(l))) <> 0) then ((*print "Absyn.VARIABLE and predicate\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(x),Absyn.LIST(l))])      
  
  | mgu (Absyn.VARIABLE(y)) (Absyn.LISTBAR(x,z1)) = (if ((searchright (Absyn.VARIABLE(y)) (Absyn.LISTBAR(x,z1))) <> 0) then ((*print "listbar and variable\n";*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else [(Absyn.VARIABLE(y),Absyn.LISTBAR(x,z1))])
  | mgu (Absyn.LISTBAR(x,z1)) (Absyn.VARIABLE(y)) = mgu (Absyn.VARIABLE(y)) (Absyn.LISTBAR(x,z1))
  | mgu (Absyn.LISTBAR(x,y)) (Absyn.LISTBAR(z1,z2)) = 
    let
      val i = ref 0;
      val rhos = ref [];
      val currho = ref [];
    in
          currho := mgu (subst (!rhos) (x)) (subst (!rhos) (z1));
          if(check_failunify(!currho)) then (rhos := (!currho)) else (
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
          currho := mgu (subst (!rhos) (y)) (subst (!rhos) (z2));
          if (check_failunify(!currho)) then (rhos := (!currho)) else (
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ));
        (!rhos)
      
    end
  | mgu (Absyn.LIST(z::l)) (Absyn.LISTBAR(x,y)) = 
    let
      val i = ref 0;
      val rhos = ref [];
      val currho = ref [];
    in
          currho := mgu (subst (!rhos) (z)) (subst (!rhos) (x));
          if(check_failunify(!currho)) then (rhos := (!currho)) else (
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
          currho := mgu (subst (!rhos) (Absyn.LIST(l))) (subst (!rhos) (y));
          if (check_failunify(!currho)) then (rhos := (!currho)) else (
          (*print "entered\n";*)
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ));
			(*print "inside list and listbar\n";*)
        (!rhos)
    end
  | mgu (Absyn.LISTBAR(x,y)) (Absyn.LIST(z)) = mgu (Absyn.LIST(z)) (Absyn.LISTBAR(x,y))
  | mgu (Absyn.VARIABLE(x)) (y) = ([(Absyn.VARIABLE(x),y)])
  | mgu (x) (Absyn.VARIABLE(y)) = ([(Absyn.VARIABLE(y),x)])
  | mgu (Absyn.UNARY_OP(x,l1)) (Absyn.UNARY_OP(y,l2)) = (if (x=y) then (mgu l1 l2) else ((*print "unary_op and unary_op\n";*)[(Absyn.BOOL(false),Absyn.BOOL(false))]))
  | mgu (Absyn.BINARY_OP(x,l1,l2)) (Absyn.BINARY_OP(y,l3,l4)) = 
  (
    let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref [];
  in
    if (x <> y) then ((*print "binary_op and binary_op\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else 
    (
        currho := mgu (subst (!rhos) (l1)) (subst (!rhos) (l3));
        if(check_failunify(!currho)) then (rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
        currho := mgu (subst (!rhos) (l2)) (subst (!rhos) (l4));
        if (check_failunify(!currho)) then (rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho)) ));
      (!rhos)
    )
  end
  )
  | mgu (Absyn.PREDICATE(x,l1)) (Absyn.PREDICATE(y,l2)) = 
  (
    let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref [];
  in
    if (x <> y) then ((*print "PREDICATE and PREDICATE\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]) else 
    (
      if((length l1) <> (length l2))then ((*print "predicate and predicate length : ";print (Int.toString(length l1));print " :: ";print (Int.toString(length l2));print "\n"; *)[(Absyn.BOOL(false),Absyn.BOOL(false))])else(
        while( (!i) < (length l1)) do 
        (
          currho := mgu (subst (!rhos) (List.nth(l1,(!i)))) (subst (!rhos) (List.nth(l2,(!i))));
          if (check_failunify(!currho)) then (i := (length l1); rhos := (!currho)) else (
          rhos := ((common((!rhos),(!currho),[]))@(!currho)) ;
          i := (!i)+1)
        );
        (!rhos)
      )
    )
  end
  )
  | mgu (Absyn.NUM(x)) (Absyn.NUM(y)) = (if (x=y) then [] else ((*print "num and num\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))])) 
  | mgu (Absyn.REAL(x)) (Absyn.REAL(y)) = (if Real.==(x, y) then [] else ((*print "real and real\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]))
  | mgu (Absyn.BOOL(x)) (Absyn.BOOL(y)) = (if (x=y) then [] else ((*print "BOOL and BOOL\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]))
  | mgu (Absyn.STRING(x)) (Absyn.STRING(y)) = (if (x=y) then [] else ((*print "STRING and STRING\n" ;*)[(Absyn.BOOL(false),Absyn.BOOL(false))]))
  | mgu (Absyn.LIST(l1)) x = mgu (Absyn.PREDICATE("0",l1)) x
  | mgu x (Absyn.LIST(l)) = mgu x (Absyn.PREDICATE("0",l))
  | mgu _ _ = ([(Absyn.BOOL false, Absyn.BOOL false)])
 

fun mgulist termlist = 
  let
    val i = ref 0;
    val rhos = ref [];
    val currho = ref[];

  in
    if ((length termlist) < 2) then [] else(
      while((!i) < ((length termlist)-1)) do
      (
        currho := mgu (subst (!rhos) (List.nth(termlist,(!i)))) (subst (!rhos) (List.nth(termlist,(!i)+1)));
        if(check_failunify(!currho)) then ((i) := ((length termlist));(*print "failed mgulist\n";*) rhos := (!currho)) else (
        rhos := ((common((!rhos),(!currho),[]))@(!currho));
        i := (!i)+1)
      );

      (!rhos)
    )
  end

fun change_vars ([],str) = []
  | change_vars (Absyn.VARIABLE(x)::ls,str) = Absyn.VARIABLE(str^x)::change_vars(ls,str)
  | change_vars (Absyn.UNARY_OP(x,l)::ls,str) = Absyn.UNARY_OP(x,List.hd(change_vars([l],str)))::change_vars(ls,str)
  | change_vars (Absyn.BINARY_OP(x,l1,l2)::ls,str) = Absyn.BINARY_OP(x,List.hd(change_vars([l1],str)),List.hd(change_vars([l2],str)))::change_vars(ls,str)
  | change_vars (Absyn.PREDICATE(x,l)::ls,str) = Absyn.PREDICATE(x,change_vars(l,str))::change_vars(ls,str)
  | change_vars (Absyn.LIST(l)::ls,str) = Absyn.LIST(change_vars(l,str))::change_vars(ls,str)
  | change_vars (Absyn.LISTBAR(x,y)::ls,str) = Absyn.LISTBAR(List.hd(change_vars([x],str)), List.hd(change_vars([y],str)))::change_vars(ls,str)
  | change_vars (x::ls,str) = x::change_vars(ls,str)

val solution = ref false;
val index = ref 0;

fun solve (fr_list,[],counter,current_binding,[],ans,str) = (solution := false;ans@current_binding)
  | solve (fr_list,x::ls,counter,current_binding,[],ans,str) = 
    let
      val i = ref 0;
      val temp_rho = ref [];
      val fr_stack : (Absyn.predicate * Absyn.predicate list) list ref = ref fr_list;
      val new_c = ref counter;
      val cur_bind = ref current_binding;
      val bind = ref [];
      val new_ans = ref (#2(Stack.top(fr_list)));
    in

      fr_stack := (List.map (fn(a) => (List.hd(change_vars([#1(a)],str)),change_vars(#2(a),str))) (!fr_stack));
      while((!i) < counter)do
      (
        fr_stack := Stack.pop(!fr_stack);
        i := (!i)+1
      );
      i := 0;
         (*print "entry\n"; *)
      while((length (!fr_stack)) > 0)do
      (
      	(*print "equate : ";
      	print (printpred (subst current_binding ((x))));
      	print "\n";
      	print (printpred (subst current_binding (#1(Stack.top(!fr_stack)))));
      	print "\n";*)
        temp_rho := (mgu (subst current_binding ((x))) (subst current_binding (#1(Stack.top(!fr_stack)))));
        if(check_failunify(!temp_rho))then 
          (fr_stack := Stack.pop(!fr_stack);
           new_c := (!new_c)+1)
        else
          (cur_bind := ((common((!cur_bind),(!temp_rho),[]))@(!temp_rho));
          	(*print "solution : " ;
          	print (Int.toString(!new_c));
          	print "\n";*)
          bind := (x,current_binding,(!cur_bind),(!new_c)+1)::(!bind);
          i := (length (!fr_stack));
          new_ans := (#2(Stack.top(!fr_stack)));
          fr_stack := [])
      );
         (*print "exit\n"; *)
      if( (!i) = 0)then ((*print "no solution found\n";*)if(!solution)then(solution := false;if((length ans)=0)then(ans@[(Absyn.VARIABLE("1"),Absyn.VARIABLE("1"))])else(ans)) else(ans@[(Absyn.VARIABLE("0"),Absyn.VARIABLE("0"))])) else ((*print "expect\n";*)
      solve(fr_list,((!new_ans))@ls,0,(!cur_bind),(!bind),(ans),str^"0"))
    end
  | solve(fr_list,[],counter,current_binding,(x::ls) : (Absyn.predicate * ((Absyn.predicate * Absyn.predicate) list) * ((Absyn.predicate * Absyn.predicate) list) * int) list ,ans,str) = ((*print "found solution : ";print (Int.toString(length current_binding));print "\n";print (Int.toString(#4(x)));print "\n";*) solution := true;if((!index) < 100)then(index := (!index)+1;solve(fr_list,[#1(x)],#4(x),#2(x),ls,(ans@current_binding),str))else((ans@current_binding)))
  | solve(fr_list,x::l1,counter,current_binding,y::l2,ans,str) =
      let
        val fr_stack = ref fr_list;
        val cur_bind= ref current_binding;
        val temp_rho = ref [];
        val new_c = ref counter;
        val bind = ref (y::l2);
        val i = ref 0;
        val new_ans = ref (Stack.top(fr_list));
      in
        fr_stack := (List.map (fn(a) => (List.hd(change_vars([#1(a)],str)),change_vars(#2(a),str))) (!fr_stack));
        (* print "entry1\n"; *)
        while((!i) < counter)do
        (
          fr_stack := Stack.pop(!fr_stack);
          i := (!i)+1
        );
        i := 0;
        while((length (!fr_stack)) > 0)do
        (
          (*print "equate22222 : ";
	      	print (printpred (subst current_binding ((x))));
	      	print "\n";
	      	print (printpred (subst current_binding (#1(Stack.top(!fr_stack)))));
	      	print "\n";*)
	          temp_rho := (mgu (subst current_binding ((x))) (subst current_binding (#1(Stack.top(!fr_stack)))));
         (*print (printpred (#1(List.hd(!temp_rho))));
         print "\n";*)
          if(check_failunify(!temp_rho))then
          (
            fr_stack := Stack.pop(!fr_stack);
             new_c := (!new_c)+1
          )
          else
          (
            cur_bind := ((common((!cur_bind),(!temp_rho),[]))@(!temp_rho)); 
            (*print "solution2 : " ;
          	print (Int.toString(!new_c));
          	print "\n";*)
            bind := (x,current_binding,(!cur_bind),(!new_c)+1)::(!bind); 
            i := (length (!fr_stack));
            new_ans := (Stack.top(!fr_stack));
            fr_stack := []
          )
        );
        if((!i) = 0)then
        (
          solve(fr_list,(#1(y))::(x::l1),#4(y),#2(y),l2,ans,String.extract(str,1,NONE))
        )
        else
        (
         (*print "exit1 : ";
         print (Int.toString(length (#2(!new_ans)@l1)));
         print "\n"; *)
          solve(fr_list,(#2(!new_ans))@l1,0,(!cur_bind),(!bind),(ans),str^"0")
        )
      end


fun main ([],_,bindings) = [(Absyn.VARIABLE("0"),Absyn.VARIABLE("0"))]
  | main (x::ls,[],bindings) = (bindings)
  | main (l1: (Absyn.predicate * (Absyn.predicate list)) list ,y::l2,bindings) = (index := 0;main(l1,l2,(solve (l1,y,0,[],[] : (Absyn.predicate * ((Absyn.predicate * Absyn.predicate) list) * ((Absyn.predicate * Absyn.predicate) list) * int) list ,[],"0"))@bindings))



fun vars (Absyn.VARIABLE(x)::l) = ([Absyn.VARIABLE(x)]@(vars l))
  | vars (Absyn.UNARY_OP(x,l)::l2) = (vars [l])@(vars l2)
  | vars (Absyn.BINARY_OP(x,l1,l2)::l3) = ((vars [l1])@(vars [l2])@(vars l3)) 
  | vars (Absyn.PREDICATE (x,l)::l2) = 
    let
      val i = ref 0;
      val ans = ref [];
    in
      while((!i)<(length l))do(ans := ((vars [(List.nth (l,(!i)))])@(!ans)); i := (!i)+1);
      (!ans)@(vars l2)
    end
  | vars (Absyn.LIST(l)::l2) = 
    let
      val i = ref 0;
      val ans = ref [];
    in
      while((!i)<(length l))do(ans := ((vars [(List.nth (l,(!i)))])@(!ans)); i := (!i)+1);
      (!ans)@(vars l2)
    end
  | vars (Absyn.LISTBAR(x,y)::ls) = (vars([x]))@(vars([y]))@(vars(ls))
  | vars _ = []

fun conv [] = []
  | conv (x::l) = (x@(conv l))




fun remove x [] = [x]
  | remove x (y::ls) = (if((predeq x y)=0)then (remove x ls)else(y::(remove x ls)))



fun returnall ([]) (Absyn.VARIABLE(x)) = ([])
  | returnall (((Absyn.VARIABLE(a) , b)::ls)) (Absyn.VARIABLE(x)) = (if (a = x) then (b::(returnall ls (Absyn.VARIABLE(x)))) else (returnall (ls) (Absyn.VARIABLE(x))))
  | returnall (_::ls) (Absyn.VARIABLE(x)) = (returnall ls (Absyn.VARIABLE(x)))
  | returnall a b = (raise invalid_returnall)

fun convert [] = "true"
	| convert [(Absyn.BOOL false, Absyn.BOOL false)] = "false"
	| convert [(Absyn.VARIABLE(x),Absyn.VARIABLE(y))] = x^" = "^y
	| convert [(Absyn.VARIABLE(x),Absyn.NUM(y))] = x^" = "^ (Int.toString(y))
	| convert [(Absyn.VARIABLE(x),Absyn.REAL(y))] = x^" = "^ (Real.toString(y))
	| convert [(Absyn.VARIABLE(x),Absyn.BOOL(y))] = x^" = "^(Bool.toString(y))
	| convert [(Absyn.VARIABLE(x),Absyn.STRING(y))] = x^" = "^y
	| convert [(Absyn.VARIABLE(x),Absyn.UNARY_OP(y,l))] = x^" = "^ (printpred (Absyn.UNARY_OP(y,l)))
	| convert [(Absyn.VARIABLE(x),Absyn.BINARY_OP(y,l1,l2))] = x^" = "^ (printpred (Absyn.BINARY_OP(y,l1,l2)))
	| convert [(Absyn.VARIABLE(x),Absyn.PREDICATE(y,l))] = x^" = "^ (printpred (Absyn.PREDICATE(y,l)))
	| convert [(Absyn.VARIABLE(x),Absyn.LIST(l))] = x^" = "^ (printpred (Absyn.LIST(l)))
	| convert [(Absyn.VARIABLE(x),Absyn.LISTBAR(y,l))] = x^" = "^(printpred (Absyn.LISTBAR(y,l)))
	| convert a = "invalid conversion"


fun finalcheck(fr_list,goals,bindings) = 
  let
    val i = ref 0;
    val variables = ref [];
    val ans = ref [];
    val temp = ref ([]);
    val j = ref 0;
    val str = ref "";
  in
    
    variables := (vars (conv goals));
    while((!i) < (length (!variables)))do
    (
    	variables := (remove (List.nth ((!variables),(!i))) (!variables));
      i := (!i)+1
    );
    i := 0;
    while((!i) < (length (!variables)))do
    (
      j := 0;
      temp := (returnall bindings (List.nth ((!variables), (!i))));
      while((!j)<(length (!temp)))do
      (
        if((predeq (List.nth ((!temp),(!j))) (List.nth ((!variables),(!i))))=0)then(j := (!j)+1)
        else
        (
          ans := (((List.nth ((!variables),(!i))),(List.nth ((!temp),(!j))))::(!ans));
          j := (!j)+1
        )
      );
      i := (!i)+1
    );
    i := 0;
    while((!i) < (length bindings))do
    (
      if((predeq (#1(List.nth ((bindings),(!i)))) (Absyn.VARIABLE("0"))) = 0)then (ans := (!ans)@([(Absyn.BOOL(false),Absyn.BOOL(false))]); i := (!i)+1)
      else if((predeq (#1(List.nth ((bindings),(!i)))) (Absyn.VARIABLE("1"))) = 0)then (ans := (!ans)@([(Absyn.BOOL(true),Absyn.BOOL(true))]); i := (!i)+1)
      else(i := (!i)+1)
    );
    (!ans)
  end

end
