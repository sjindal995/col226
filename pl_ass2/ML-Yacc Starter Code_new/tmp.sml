fun query(goal,fact_list , rule_list) =
  let
    val i = ref 0;
    val g = ref 0;
    val fact_stack : Absyn.predicate list ref = ref [];
    val rule_stack : (Absyn.predicate * Absyn.predicate list) list ref= ref [];
    val ans = ref [];
    val curans = ref [];
    
    
  in
    print "before loop : ";
    print (Int.toString(!g));print "\n";
    while ((!g) < (length goal)) do
    (
        i := 0;
        fact_stack := fact_list;
        rule_stack := rule_list;
        while ((length (!fact_stack)) <> 0 ) do
        (
          let     
            val temp = Stack.top(!fact_stack) ;
          in
            curans := (mgu (subst (!ans) temp) (subst (!ans) (List.nth (goal, (!g)))));
            if (check_failunify(!curans)) then (print "fail\n";fact_stack := Stack.pop(!fact_stack))
          else
            (ans := ((common((!ans),(!curans),[]))@(!curans));
              print "success\n";
           fact_stack := [];
            rule_stack := [])
          end

        );
        while ((length (!rule_stack)) <> 0 ) do
        (
          let     
            val temp = #1(Stack.top(!rule_stack)) ;
            val subgoals = ref [];
            val uni = ref [];
          in
            curans := (mgu temp (List.nth (goal, (!g))));
            if(check_failunify(!curans)) then (print "rule_fail\n";rule_stack := Stack.pop(!rule_stack))
            else(
              print "rule_success\n";
              subgoals := #2(Stack.top(!rule_stack));
              uni := query(!subgoals,fact_list,rule_list);
              if((length (!uni)) = 0) then (rule_stack := Stack.pop(!rule_stack))
              else(ans := ((common((!ans),(!curans),[]))@(!curans)); rule_stack := [])
            )
          end
        );
        print "g : ";
        print (Int.toString(!g));print "\n";
        print (Int.toString(!g));print "\n";
        g := (!g)+1
    );
     (!ans)
  end
