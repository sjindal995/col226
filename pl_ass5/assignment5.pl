lookup([],_,_) :- fail.
lookup([(var(X),T)|_],var(X),T) :- !.
lookup([_|Gamma],var(X),T) :- lookup(Gamma,var(X),T).

arithmetic(integer).
arithmetic(real).

hastype(Gamma,var(X),T) :- lookup(Gamma,var(X),T).
hastype(_,unit,unit).
hastype(_,num(_),integer).
hastype(_,mybool(_),bool).
hastype(_,myreal(_),real).
hastype(_,string(_),string).

hastype(Gamma,plus(E1,E2),A) :- hastype(Gamma,E1,A),hastype(Gamma,E2,A),arithmetic(A).
hastype(Gamma,sub(E1,E2),A) :- hastype(Gamma,E1,A),hastype(Gamma,E2,A),arithmetic(A).
hastype(Gamma,times(E1,E2),A) :- hastype(Gamma,E1,A),hastype(Gamma,E2,A),arithmetic(A).
hastype(Gamma,intdiv(E1,E2),integer) :- hastype(Gamma,E1,integer),hastype(Gamma,E2,integer).
hastype(Gamma,realdiv(E1,E2),real) :- hastype(Gamma,E1,A),hastype(Gamma,E2,A),arithmetic(A).

hastype(Gamma,or(E1,E2),bool) :- hastype(Gamma,E1,bool),hastype(Gamma,E2,bool).
hastype(Gamma,and(E1,E2),bool) :- hastype(Gamma,E1,bool),hastype(Gamma,E2,bool).
hastype(Gamma,neg(E1),bool) :- hastype(Gamma,E1,bool).
	
hastype(Gamma,gt(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).
hastype(Gamma,eq(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).
hastype(Gamma,lt(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).
hastype(Gamma,neq(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).
hastype(Gamma,geq(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).
hastype(Gamma,leq(E1,E2),bool) :- hastype(Gamma,E1,T),hastype(Gamma,E2,T),arithmetic(T).


hastype(Gamma,concat(E1,E2),string) :- hastype(Gamma,E1,string),hastype(Gamma,E2,string).
hastype(Gamma,subStr(E1,E2,E3),string) :- hastype(Gamma,E1,string),hastype(Gamma,E2,integer),hastype(Gamma,E3,integer).

hastype(_,record([]),rec([])).
hastype(Gamma,record([(X,E1)|E2]),rec([(X,T1)|T])) :- hastype(Gamma,E1,T1),hastype(Gamma,record(E2),rec(T)).

hastype(Gamma,if_then_else(E1,E2,E3),T) :- hastype(Gamma,E1,bool),hastype(Gamma,E2,T),hastype(Gamma,E3,T).
hastype(Gamma,tuple(E1,E2),cross(T1,T2)) :- hastype(Gamma,E1,T1),hastype(Gamma,E2,T2).
hastype(Gamma,lambda(var(X),E),arrow(T1,T2)) :- hastype([(var(X),T1)|Gamma],E,T2).
hastype(Gamma,lambda(tuple(X,Y),E),arrow(cross(T1,T2),T3)) :- type_table_bind(tuple(X,Y),cross(T1,T2),DGamma),append(DGamma,Gamma,NGamma),hastype(NGamma,X,T1),hastype(NGamma,Y,T2),hastype(NGamma,E,T3).
hastype(Gamma,app(E1,E2),T2) :- hastype(Gamma,E1,arrow(T1,T2)),hastype(Gamma,E2,T1).
hastype(Gamma,let_in_end(D,E),T) :- type_elaborate(Gamma,D,Gamma1),append(Gamma1,Gamma,NGamma),hastype(NGamma,E,T).


type_table_bind(var(X),Y,[(var(X),Y)]).
type_table_bind(tuple(X,Y),cross(T1,T2),[(X,T1)|G]) :- type_table_bind(Y,T2,G).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% subject reduction theorem
%% hastype(Gamma,A,T) :- calculates(Gamma,E,A),hastype(Gamma,E,T).
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%


type_elaborate(Gamma,binding(X,E),[(X,T)]) :- hastype(Gamma,E,T).
type_elaborate(Gamma,seq(E1,E2),T) :- type_elaborate(Gamma,E1,G1),append(G1,Gamma,NG),type_elaborate(NG,E2,G2),append(G2,G1,T).
type_elaborate(Gamma,parallel(E1,E2),T) :- type_elaborate(Gamma,E1,G1),type_elaborate(Gamma,E2,G2),append(G1,G2,T).
type_elaborate(Gamma,local(D1,D2),T) :- type_elaborate(Gamma,D1,G1),append(G1,Gamma,NG),type_elaborate(NG,D2,T).


calculates(_,unit,unit).
calculates(Gamma,var(X),T) :- lookup(Gamma,var(X),T).
calculates(_,num(N),N).
calculates(_,mybool(X),X).
calculates(_,myreal(X),X).
calculates(_,string(X),A) :- string_codes(A,X).

calculates(Gamma,plus(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A is A1+A2.
calculates(Gamma,sub(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A is A1-A2.
calculates(Gamma,times(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A is A1*A2.
calculates(Gamma,intdiv(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A is A1//A2.
calculates(Gamma,realdiv(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A is A1/A2.


calculates(Gamma,or(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1;A2).
calculates(_,or(_,_),false).
calculates(Gamma,and(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),A1,A2.
calculates(_,and(_,_),false).
calculates(Gamma,neg(E1),false) :- calculates(Gamma,E1,true).
calculates(_,neg(_),true).

calculates(Gamma,gt(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1>A2).
calculates(Gamma,eq(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=:=A2).
calculates(Gamma,lt(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1<A2).
calculates(Gamma,neq(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=\=A2).
calculates(Gamma,geq(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1>=A2).
calculates(Gamma,leq(E1,E2),true) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=<A2).

calculates(Gamma,gt(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=<A2).
calculates(Gamma,eq(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=\=A2).
calculates(Gamma,lt(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1>=A2).
calculates(Gamma,neq(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1=:=A2).
calculates(Gamma,geq(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1<A2).
calculates(Gamma,leq(E1,E2),false) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),(A1>A2).


calculates(Gamma,concat(E1,E2),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),string_concat(A1,A2,A).
calculates(Gamma,subStr(E1,E2,E3),A) :- calculates(Gamma,E1,A1),calculates(Gamma,E2,A2),calculates(Gamma,E3,A3),A4 is A3-A2+1,sub_string(A1,A2,A4,_,A).


calculates(_,record([]),rec([])).
calculates(Gamma,record([(X,E1)|E2]),rec([(X,T1)|T])) :- calculates(Gamma,E1,T1),calculates(Gamma,record(E2),rec(T)).

calculates(Gamma,if_then_else(E1,E2,_),A) :- calculates(Gamma,E1,true),calculates(Gamma,E2,A).
calculates(Gamma,if_then_else(E1,_,E3),A) :- calculates(Gamma,E1,false),calculates(Gamma,E3,A).
calculates(Gamma,tuple(E1,E2),tup(T1,T2)) :- calculates(Gamma,E1,T1),calculates(Gamma,E2,T2).
calculates(Gamma,lambda(X,E),closure(Gamma,lambda(X,E))).
calculates(Gamma,app(E1,E2),A) :- calculates(Gamma,E1,closure(NGamma,lambda(X,NE))),calculates(Gamma,E2,A2),tuple_bind(X,A2,NG),append(NG,NGamma,NewGamma),calculates(NewGamma,NE,A).
calculates(Gamma,let_in_end(D,E),A) :- val_elaborate(Gamma,D,Gamma1),append(Gamma1,Gamma,NGamma),calculates(NGamma,E,A).

tuple_bind(var(X),G,[(var(X),G)]).
tuple_bind(tuple(var(X1),X2),tup(A1,A2),[(var(X1),A1)|G]) :- tuple_bind(X2,A2,G).


val_elaborate(Gamma,binding(X,E),[(X,T)]) :- calculates(Gamma,E,T).
val_elaborate(Gamma,seq(E1,E2),T) :- val_elaborate(Gamma,E1,G1),append(G1,Gamma,NG),val_elaborate(NG,E2,G2),append(G2,G1,T).
val_elaborate(Gamma,parallel(E1,E2),T) :- val_elaborate(Gamma,E1,G1),val_elaborate(Gamma,E2,G2),append(G1,G2,T).
val_elaborate(Gamma,local(D1,D2),T) :- val_elaborate(Gamma,D1,G1),append(G1,Gamma,NG),val_elaborate(NG,D2,T).

