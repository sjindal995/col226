Control.Print.printLength := 10000;
Control.Print.printDepth := 10000;
open TextIO;
exception INVALID_FUNCTION
exception INVALID_CALL
datatype node = NODE of string*node*((string* (int ref)) list)*((string * (int ref)) list)*(int list)*int
	| EMPTY

val main = NODE("main",EMPTY,[],[("x1",ref 0),("x2",ref 0),("x3",ref 0),("x4",ref 0)],[1,5],0)

val p = NODE("p",main,[("x1",ref 0),("y1",ref 0)],[("x2",ref 0),("y2",ref 0)],[2,4],1)

val r = NODE("r",p,[("x3",ref 0),("y4",ref 0)],[("x4",ref 0),("y3",ref 0)],[3],2)

val v = NODE("v",r,[("x1",ref 0),("y1",ref 0)],[],[],3)

val s = NODE("s",p,[("x2",ref 0),("y2",ref 0)],[("x3",ref 0),("y1",ref 0)],[],4)

val q = NODE("q",main,[("x2",ref 0),("y3",ref 0)],[("x1",ref 0),("y4",ref 0)],[6,8],5)

val t = NODE("t",q,[("x1",ref 0),("y3",ref 0)],[("x3",ref 0),("y1",ref 0)],[7],6)

val w = NODE("w",t,[("x4",ref 0),("y2",ref 0)],[("x1",ref 0)],[],7)

val u = NODE("u",q,[("x3",ref 0),("y2",ref 0)],[("x1",ref 0),("y4",ref 0)],[],8)

val nodes = [main,p,r,v,s,q,t,w,u]

fun getFormalParameters(NODE(_,_,ls,_,_,_)) = (List.map (fn a => (#1(a),(!(#2(a))))) ls)

fun getLocalVariables(NODE(_,_,_,ls,_,_)) = (List.map (fn a => (#1(a),(!(#2(a))))) ls)

fun getParent(NODE(_,x,_,_,_,_))=x

fun getChildren(NODE(_,_,_,_,ls,_)) = (List.map (fn a => (List.nth(nodes,a))) ls)

fun getIndex(NODE(_,_,_,_,_,x)) = x

fun nodeEq(EMPTY,EMPTY) = true
  | nodeEq(_,EMPTY) = false
  | nodeEq(EMPTY,_) = false
  | nodeEq(x,y) = (getIndex(x)=getIndex(y)) 

fun getLevel(NODE(_,x,_,_,_,_)) = (if(nodeEq(x,EMPTY))then(0)else(1+getLevel(x)))

fun getNode(x,[]) = (raise INVALID_FUNCTION)
  | getNode(x,NODE(y,x1,x2,x3,x4,x5)::ls) = (if(x=y)then(NODE(y,x1,x2,x3,x4,x5))else((*print x;print " != ";print y;print "\n";*)getNode(x,ls)))

fun getString(NODE(x,_,_,_,_,_)) = x

fun shadowVar(ls1,[]) = []
  | shadowVar(ls1 : (string * (int ref)) list,x::ls2 : (string * (int ref)) list) = (if((List.find (fn a => (#1(x) = #1(a))) (ls1))=NONE)then(x::shadowVar(ls1,ls2))else(shadowVar(ls1,ls2)) )

fun visibleVarRef(NODE(_,EMPTY,ls1,ls2,_,_)) = ls2@shadowVar(ls2,ls1)
  | visibleVarRef(NODE(_,x,ls1,ls2,_,_)) = ls2@shadowVar(ls2,ls1)@shadowVar(ls2@shadowVar(ls2,ls1),visibleVarRef(x))

fun visibleVars(x) = (List.map (fn a => (#1(a),(!(#2(a))))) (visibleVarRef(x)))

fun callableProNodes(x) = if(nodeEq(x,main))then((getChildren(x)))else(((getChildren(x)))@callableProNodes(getParent(x)))

fun callableProString(x) = (List.map (fn a => getString(a)) (callableProNodes(x)))


fun getInput() = (String.tokens (fn a => ((a = #" ")orelse(a = #"\n"))) ((TextIO.input (TextIO.stdIn))))

structure Stack = 
struct
exception emptyStack;
val new = [];
fun isempty x = (x = []);
fun push x s = x::s;
fun pop (x::s) = s
  | pop [] = raise emptyStack;
fun top (x::s) = x
  | top [] = raise emptyStack;
end

fun checkCallable(x,y) = not((List.find (fn a => (x=a)) (callableProNodes(y)))=NONE)

fun buildStack() = 
	let
	 	val inp = List.rev(getInput());
	 	val prostack = ref (List.map (fn a => getNode(a,nodes)) inp);
	 	val i = ref 1;
	 in
	 	while((!i)<(length (!prostack)))do
	 	(
	 		(*print (getString(List.nth ((!prostack),(!i - 1))));
	 			print " called by ";
	 			print (getString(List.nth ((!prostack),(!i))));*)
	 		if(checkCallable((List.nth ((!prostack),(!i - 1))),(List.nth ((!prostack),(!i)))))then()else(raise INVALID_CALL);
	 		i := (!i)+1
	 	);
	 	if(not(nodeEq(List.last (!prostack), main)))then(raise INVALID_CALL)else();
	 	(inp)

	 end 


val global_stack : node list ref = ref [];
(*val vars : (string * int ref)list ref = ref [];*)


fun callPro(x) = (if(length (!global_stack) = 0)then(if(nodeEq(getNode(x,nodes),main))then(global_stack := [main];print "success!\n")else(print "call failed!\n"))else(if(checkCallable(getNode(x,nodes),Stack.top(!global_stack)))then(global_stack := (Stack.push (getNode(x,nodes)) (!global_stack));print "success!\n")else(print "call failed!\n") );
	(*vars := (visibleVarRef(Stack.top (!global_stack)));*)
	 (List.map (fn a => (getString(a))) (!global_stack)))


fun ret() = (if((length (!global_stack))=0)then(print "no function to return from!\n")else(global_stack := (Stack.pop(!global_stack));print "success!\n"))


fun passParam(NODE(a1,a2,ls,a3,a4,a5),ls2) = 
	let
		val i = ref 0;
		
	in
		while((!i)<(length ls))do(
			#2(List.nth(ls,(!i))) := (valOf(Int.fromString(List.nth(ls2,(!i)))))
		);
		NODE(a1,a2,ls,a3,a4,a5)
	end
(*fun passParam(n,ls1) = 
	let
		val i = ref 0
	in
		(List.map (fn a => ((#2(a) := (List.nth(ls1,(!i)))); i := (!i)+1)) (getFormalParameters(Stack.top(!global_stack))))
	end
*)
fun parameterCall(x) = (
	let
		val name = (String.tokens (fn a => ((a = #" ")orelse(a = #"\n")orelse(a = #",")orelse(a = #"(")orelse(a = #")"))) (x));
	in
		callPro(List.nth(name,0));
		passParam(Stack.top(!global_stack),List.tl(name))
	end
	)


(*fun changeNode(x,y,n) = 
	(
		if((List.find (fn a => (#1(a) = x)) lv)=NONE)then()else(marked_node := n)
	)
	


fun changeVar(x,y) = (if((List.find (fn a => (a=x)) (visibleVars(Stack.top(!global_stack))))=NONE)then(print "variable not found!\n")else(changeNode(x,y,Stack.top(!global_stack))))*)