Control.Print.printLength := 1000;
open TextIO;
use "assignment1.lex.sml";
open MathLex;
open UserDeclarations;
val inp = openIn("test.txt");
val l : Internal.result list = [];
val c = ref 1;
val str = ref "";
str := input(inp);
fun endfile() = 
  if !c = 1 then (str := input(inp); c := !c + 1) else (str := "");

val lexer = (makeLexer( fn n => (!str)));

val inplexer = (makeLexer( fn n => (valOf(inputLine(stdIn)))));

fun nexttoken() = lexer();




fun main(tokenlist,EOF) = ( tokenlist)
  | main(tokenlist,token) = ((str := ""); main(tokenlist@[token], lexer()));

fun main1(tokenlist,token) = 
  case token of
    EOF => ( tokenlist)
    |_  => (endfile(); main1(tokenlist@[token], nexttoken()));

val printlist = (fn _ => main1([],lexer()));