datatype lexresult = PLUS | SUB | TIMES | INTDIV | REALDIV | GREAT | LESS | GREATEQ | LESSEQ | EQ | NEQ | CAT | BOOL of bool | REAL of real | STRING of string | SMALLID of string | CAPID of string | NUM of int | SUBLEFT of int | SUBRIGHT of int | LPAREN | RPAREN | IF | THEN | ELSE | LET | END | COMMA | NEGATION | COLON | MOD | ABS | EXPONENT | CONJUNCT | DISJUNCT | EOF | DOT | FORALL | EXISTS

val error = fn x => TextIO.output(TextIO.stdOut,x ^ "\n")
val eof = fn () => EOF
val strsize : int ref = ref 0;

%%
%structure MathLex
ints = [1-9][0-9]*;
lcid = [a-z]([A-Za-z'_] | [0-9])*;
ucid = [A-Z]([A-Za-z'_] | [0-9])*;
ws = [\ \t\n];
digit = [0-9];
nzdigits = [1-9];
frac = ("."{digit}*{nzdigits}) | ".0" ;
expo = "E"((~?){ints} | "0");
reals = {ints}{frac}{expo}?;
%%
"+"      => (PLUS);
"-"      => (SUB);
"*"      => (TIMES);
"div"	=> (INTDIV);
"/"      => (REALDIV);
"%"  => (MOD);
"abs" => (ABS);
">" => (GREAT);
"<" => (LESS);
">=" => (GREATEQ);
"<=" => (LESSEQ);
"==" => (EQ);
"<>" => (NEQ);
{ws}+    => (lex());
"if" => (IF);
"then" => (THEN);
"else" => (ELSE);
"let" => (LET);
"end" => (END);
"," => (COMMA);
":" => (COLON);
"&" => (CONJUNCT);
"||" => (DISJUNCT);
"~" => (NEGATION);
"forall" => (FORALL);
"exists" => (EXISTS);
"." => (DOT);


"exp" => (EXPONENT);

("~")?{ints}	=> (NUM (valOf(Int.fromString(yytext))));
"0"	=> (NUM 0);

"true"	=> (BOOL true);
"false" => (BOOL false);
"\""[^"\""]*"\"" => ((strsize := ((size yytext)-2)) ; (STRING (String.substring(yytext,1,(size yytext)-2))));
"^" => (CAT);
"["({ints}|"0") => (SUBLEFT (foldl (fn(a,r)=>ord(a)-ord(#"0")+10*r) 0 (explode (String.substring(yytext,1,(size yytext)-1)))));
".."({ints}|"0")"]" => (SUBRIGHT (foldl (fn(a,r)=>ord(a)-ord(#"0")+10*r) 0 (explode (String.substring(yytext,2,(size yytext)-3)))));
"...]" => (SUBRIGHT ((!strsize) - 1));


"(" => (LPAREN);
")" => (RPAREN);

"0.0" => (REAL 0.0);
("~")?{reals} => (REAL (valOf(Real.fromString(yytext))));
{lcid} => (SMALLID yytext);
{ucid} => (CAPID yytext);
.	=> (error ("MathLex: ignoring bad character " ^ yytext); lex());