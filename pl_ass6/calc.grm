(*val fact_rules : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goals : (Absyn.predicate list) list ref = ref [];
val fr_list : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goal_list : (Absyn.predicate list) list ref = ref [];*)
%%

%eop EOF SEMI
%pos int
%nonassoc GREAT LESSTHAN GREATEQ LESSEQ
%left EQ NEQ
%right CAT ABS IF NEGATION CONJUNCT DISJUNCT
%left PLUS SUB
%left MOD
%left TIMES INTDIV REALDIV
%term
    PLUS | SUB | TIMES | INTDIV | REALDIV | GREAT | LESSTHAN | GREATEQ | LESSEQ | EQ | NEQ | CAT | BOOL of bool | REAL of real | STRING of string | SMALLID of string | CAPID of string | NUM of int | LPAREN | RPAREN | IF | THEN | ELSE | LET | END | COMMA | NEGATION | COLON | MOD | ABS | EXPONENT | CONJUNCT | DISJUNCT | EOF | DOT | PRINT | SEMI | QUES | LSBRACKET | RSBRACKET | LISTBAR | LAMBDA | FUNCTION | FI | COLON_EQ | IN | EQUAL_SIGN

(* gform: goal formula
   dform: definite clause *)

%nonterm program of Absyn.exp
	| expression of Absyn.exp
	| def_seq of Absyn.exp list
  (*program of (((Absyn.predicate * Absyn.predicate list) list) * (Absyn.predicate list list))
  | declaration of (Absyn.predicate * Absyn.predicate list)
  | declaration_seq of (Absyn.predicate * Absyn.predicate list) list 
  | fact of Absyn.predicate
  | predicate of Absyn.predicate 
  | termsequence of Absyn.predicate list
  | rule of Absyn.predicate * Absyn.predicate list 
  | head of Absyn.predicate 
  | body of Absyn.predicate list
  | goal of Absyn.predicate list
  | predicatesequence of Absyn.predicate list
  | predicatesymbol of string
  | term of Absyn.predicate
  | expression of Absyn.predicate
  | expression_seq of Absyn.predicate list
  *)


%name Calc
%subst PRINT for SMALLID
%subst PRINT for CAPID
%keyword PRINT SEMI
%noshift EOF
%verbose
%%

program : expression							(expression)

expression : NUM                               	(Absyn.NUM NUM)
		   | CAPID 								(Absyn.VAR CAPID)
		   | SMALLID 							(Absyn.VAR SMALLID)
		   | BOOL 								(Absyn.BOOL BOOL)

		   | LPAREN expression RPAREN 			(expression)
		   | expression PLUS expression			(Absyn.PLUS (expression1,expression2))
		   | expression SUB expression 			(Absyn.SUB (expression1,expression2))
		   | expression TIMES expression 		(Absyn.TIMES (expression1,expression2))
		   | expression INTDIV expression 		(Absyn.INTDIV (expression1,expression2))
		   
		   | expression GREAT expression 		(Absyn.GT (expression1,expression2))
		   | expression GREATEQ expression 		(Absyn.GTE (expression1,expression2))
		   | expression LESSEQ expression 		(Absyn.LTE (expression1,expression2))
		   | expression LESSTHAN expression 		(Absyn.LT (expression1,expression2))
		   | expression EQ expression 			(Absyn.EQ (expression1,expression2))
		   | expression NEQ expression 			(Absyn.NEQ (expression1,expression2))
		   | NEGATION expression 			(Absyn.NOT (expression1))
		   | expression CONJUNCT expression 	(Absyn.AND (expression1,expression2))
		   | expression DISJUNCT expression 	(Absyn.OR (expression1,expression2))
		   | LET def_seq IN expression END								(Absyn.LET_IN_END(def_seq,expression))		   

		   | IF expression THEN expression ELSE expression (Absyn.IF_THEN_ELSE(expression1,expression2,expression3))

		   | LAMBDA CAPID DOT LPAREN expression RPAREN			(Absyn.LAMBDA(CAPID,expression))
		   | LAMBDA SMALLID DOT LPAREN expression RPAREN			(Absyn.LAMBDA(SMALLID,expression))
		   | expression LPAREN expression RPAREN 	(Absyn.APP(expression1,expression2))

def_seq : expression COLON_EQ expression		([Absyn.BIND(expression1,expression2)])
		| expression COLON_EQ expression COMMA def_seq 		(Absyn.BIND(expression1,expression2)::def_seq)