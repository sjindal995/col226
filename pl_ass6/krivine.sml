structure Krivine = 
	struct
		open Absyn;
		exception VARIABLE_NOT_FOUND
        exception INVALID_LOOKUP
        exception INVALID_NUM_EVAL
        exception INVALID_BOOL_EVAL

		datatype closure = CLOSURE of ((exp*closure) list) * exp

             and answer = INTANS of int                         | BOOLANS of
bool                         | CLOSANS of ((exp*answer) list)*exp
and gamma = GAMMA of (exp*closure) list

		fun lookup([],x) = (raise VARIABLE_NOT_FOUND)
		  | lookup((VAR(x),a)::ls,VAR(y)) = (if (x=y)then (a) else(lookup(ls,VAR(y))))
          | lookup (_,_) = (raise INVALID_LOOKUP)


        fun execute(CLOSURE(gamma,NUM(x)),s) = (CLOSURE(gamma,NUM(x)))
          | execute(CLOSURE(gamma,BOOL(x)),s) = (CLOSURE(gamma,BOOL(x)))
          | execute(CLOSURE(gamma,VAR(x)),s) = execute(lookup(gamma,VAR(x)),s)


          | execute(CLOSURE(gamma,PLUS(e1,e2)),s) = (CLOSURE(gamma,PLUS(e1,e2)))
          | execute(CLOSURE(gamma,SUB(e1,e2)),s) = (CLOSURE(gamma,SUB(e1,e2)))
          | execute(CLOSURE(gamma,TIMES(e1,e2)),s) = (CLOSURE(gamma,TIMES(e1,e2)))
          | execute(CLOSURE(gamma,INTDIV(e1,e2)),s) = (CLOSURE(gamma,INTDIV(e1,e2)))

          | execute(CLOSURE(gamma,GT(e1,e2)),s) = (CLOSURE(gamma,GT(e1,e2)))
          | execute(CLOSURE(gamma,LT(e1,e2)),s) = (CLOSURE(gamma,LT(e1,e2)))
          | execute(CLOSURE(gamma,GTE(e1,e2)),s) = (CLOSURE(gamma,GTE(e1,e2)))
          | execute(CLOSURE(gamma,LTE(e1,e2)),s) = (CLOSURE(gamma,LTE(e1,e2)))
          | execute(CLOSURE(gamma,EQ(e1,e2)),s) = (CLOSURE(gamma,EQ(e1,e2)))
          | execute(CLOSURE(gamma,NEQ(e1,e2)),s) = (CLOSURE(gamma,NEQ(e1,e2)))

          | execute(CLOSURE(gamma,AND(e1,e2)),s) = (CLOSURE(gamma,AND(e1,e2)))
          | execute(CLOSURE(gamma,OR(e1,e2)),s) = (CLOSURE(gamma,OR(e1,e2)))
          | execute(CLOSURE(gamma,NOT(e1)),s) = (CLOSURE(gamma,NOT(e1)))

          | execute(CLOSURE(gamma,IF_THEN_ELSE(e1,e2,e3)),s) = (CLOSURE(gamma,IF_THEN_ELSE(e1,e2,e3)))
          | execute(CLOSURE(gamma,LET_IN_END(ls,e)),s) = (CLOSURE(gamma,LET_IN_END(ls,e)))
          | execute(CLOSURE(gamma,BIND(e1,e2)),s) = (CLOSURE(gamma,BIND(e1,e2)))

          | execute(CLOSURE(gamma,LAMBDA(x,e)),CLOSURE(gamma1,e2)::s) = execute(CLOSURE((VAR(x),CLOSURE(gamma1,e2))::gamma,e),s)
          | execute(CLOSURE(gamma,APP(e1,e2)),s) = execute(CLOSURE(gamma,e1),CLOSURE(gamma,e2)::s)

        fun num_eval(NUM(a)) = a
          | num_eval(_) = (raise INVALID_NUM_EVAL)

        fun bool_eval(BOOL(a)) = a
          | bool_eval(_) = (raise INVALID_BOOL_EVAL)

        fun eval(CLOSURE(gamma,NUM(x))) = NUM(x)
          | eval(CLOSURE(gamma,BOOL(x))) = BOOL(x)
          | eval(CLOSURE(gamma,VAR(x))) = (eval(lookup(gamma,VAR(x))))

          | eval(CLOSURE(gamma,PLUS(e1,e2))) = NUM(num_eval(eval(CLOSURE(gamma,e1)))+num_eval(eval(CLOSURE(gamma,e2))))
          | eval(CLOSURE(gamma,SUB(e1,e2))) = NUM(num_eval(eval(CLOSURE(gamma,e1))) - num_eval(eval(CLOSURE(gamma,e2))))
          | eval(CLOSURE(gamma,TIMES(e1,e2))) = NUM(num_eval(eval(CLOSURE(gamma,e1))) * num_eval(eval(CLOSURE(gamma,e2))))
          | eval(CLOSURE(gamma,INTDIV(e1,e2))) = NUM((num_eval(eval(CLOSURE(gamma,e1)))) div (num_eval(eval(CLOSURE(gamma,e2)))))

          | eval(CLOSURE(gamma,GT(e1,e2))) = BOOL((num_eval(eval(CLOSURE(gamma,e1)))) > (num_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,LT(e1,e2))) = BOOL((num_eval(eval(CLOSURE(gamma,e1)))) < (num_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,GTE(e1,e2))) = BOOL((num_eval(eval(CLOSURE(gamma,e1)))) >= (num_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,LTE(e1,e2))) = BOOL((num_eval(eval(CLOSURE(gamma,e1)))) <= (num_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,EQ(e1,e2))) = BOOL(((eval(CLOSURE(gamma,e1)))) = ((eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,NEQ(e1,e2))) = BOOL(((eval(CLOSURE(gamma,e1)))) <> ((eval(CLOSURE(gamma,e2)))))

          | eval(CLOSURE(gamma,OR(e1,e2))) = BOOL((bool_eval(eval(CLOSURE(gamma,e1)))) orelse (bool_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,AND(e1,e2))) = BOOL((bool_eval(eval(CLOSURE(gamma,e1)))) andalso (bool_eval(eval(CLOSURE(gamma,e2)))))
          | eval(CLOSURE(gamma,NOT(e1))) = BOOL(not(bool_eval(eval(CLOSURE(gamma,e1)))))

          | eval(CLOSURE(gamma,IF_THEN_ELSE(e1,e2,e3))) = (if(bool_eval(eval(CLOSURE(gamma,e1))))then(eval(CLOSURE(gamma,e2)))else(eval(CLOSURE(gamma,e3))))
          (*| eval(CLOSURE(gamma,BIND(e1,e2))) = (CLOSURE((e1,CLOSURE(gamma,e2))::gamma,e2))*)
          | eval(CLOSURE(gamma,LET_IN_END([],e))) = (eval(CLOSURE(gamma,e)))
          | eval(CLOSURE(gamma,LET_IN_END(BIND(e1,e2)::ls,e))) = 
            let
                val ((gamma1)) = (e1,(CLOSURE(gamma,e2)))::gamma 
            in
                eval(CLOSURE(gamma1,LET_IN_END(ls,e)))
            end
          | eval(x) = eval(execute(x,[]))



	end