structure Stack = 
struct
exception emptyStack;
val new = [];
fun isempty x = (x = []);
fun push x s = x::s;
fun pop (x::s) = s
  | pop [] = raise emptyStack;
fun top (x::s) = x
  | top [] = raise emptyStack;
end

structure Absyn =
   struct
       datatype exp = VAR of string
                            | NUM of int
                            | BOOL of bool

                            | PLUS of exp*exp
                            | SUB of exp*exp       
                            | TIMES of exp*exp
                            | INTDIV of exp*exp

                            | GT of exp*exp
                            | GTE of exp*exp
                            | LT of exp*exp
                            | LTE of exp*exp
                            | EQ of exp*exp
                            | NEQ of exp*exp

                            | AND of exp*exp
                            | OR of exp*exp
                            | NOT of exp

                            | IF_THEN_ELSE of exp*exp*exp
                            | LET_IN_END of (exp list)*exp
                            | BIND of exp*exp

                            | LAMBDA of string*exp
                            | APP of exp*exp


end




