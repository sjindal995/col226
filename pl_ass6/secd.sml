structure Secd =
  struct
    open Absyn;
    exception VARIABLE_NOT_FOUND
    datatype opcode = OPVAR of string
                    | OPNUM of int
                    | OPBOOL of bool

                    | OPPLUS
                    | OPSUB
                    | OPTIMES
                    | OPINTDIV

                    | OPGT
                    | OPGTE
                    | OPLT
                    | OPLTE
                    | OPEQ
                    | OPNEQ

                    | OPAND
                    | OPOR
                    | OPNOT

                    | OPLET
                    | OPIN
                    | OPEND
                    | OPBIND of ((opcode list)*(opcode list))

                    | IF 
                    | THEN_ELSE of ((opcode list)*(opcode list))

                    | OPLAMBDA of opcode*(opcode list)
                    | OPAPP
                    | RET

    and answer = CLOSURE of ((exp*answer) list)*opcode*(opcode list)
               | INTANS of int
               | BOOLANS of bool


      fun lookup([],VAR(x)) = raise VARIABLE_NOT_FOUND
        | lookup((VAR(x),a)::ls,VAR(y)) = if(x=y)then(a)else(lookup(ls,VAR(y)))
        
      fun getList(NUM(x)) = [OPNUM(x)]
        | getList(BOOL(x)) = [OPBOOL(x)]
        | getList(VAR(x)) = [OPVAR(x)]

        | getList(PLUS(e1,e2)) = getList(e1)@getList(e2)@[OPPLUS]
        | getList(SUB(e1,e2)) = getList(e1)@getList(e2)@[OPSUB]
        | getList(TIMES(e1,e2)) = getList(e1)@getList(e2)@[OPTIMES]
        | getList(INTDIV(e1,e2)) = getList(e1)@getList(e2)@[OPINTDIV]

        | getList(GT(e1,e2)) = getList(e1)@getList(e2)@[OPGT]
        | getList(GTE(e1,e2)) = getList(e1)@getList(e2)@[OPGTE]
        | getList(LT(e1,e2)) = getList(e1)@getList(e2)@[OPLT]
        | getList(LTE(e1,e2)) = getList(e1)@getList(e2)@[OPLTE]
        | getList(EQ(e1,e2)) = getList(e1)@getList(e2)@[OPEQ]
        | getList(NEQ(e1,e2)) = getList(e1)@getList(e2)@[OPNEQ]

        | getList(AND(e1,e2)) = getList(e1)@getList(e2)@[OPAND]
        | getList(OR(e1,e2)) = getList(e1)@getList(e2)@[OPOR]
        | getList(NOT(e)) = getList(e)@[OPNOT]

        (*| getList(IF_THEN_ELSE(e1,e2,e3)) = getList(e3)@getList(e2)@getList(e1)@[OP_IF_THEN_ELSE]*)
        | getList(IF_THEN_ELSE(e1,e2,e3)) = getList(e1)@[IF]@[THEN_ELSE(getList(e2),getList(e3))]

        | getList(BIND(e1,e2)) = [OPBIND(getList(e1),getList(e2))]
        | getList(LET_IN_END(ls,e3)) = [OPLET]@(List.concat(List.map (getList) ls) )@[OPIN]@getList(e3)@[OPEND]

        | getList(LAMBDA(x,e)) = [OPLAMBDA(OPVAR(x),getList(e)@[RET])]
        | getList(APP(e1,e2)) = getList(e1)@getList(e2)@[OPAPP]


      val counter = ref 0;

      fun singleStep(ans_stack,gamma,[],dump) = (print "ans\n";ans_stack)

        | singleStep(ans_stack,gamma,OPVAR(x)::ls,dump) = (print "opvar\n";singleStep((lookup(gamma,VAR(x)))::ans_stack,gamma,ls,dump))
        | singleStep(ans_stack,gamma,OPNUM(x)::ls,dump) = (print "opnum\n";singleStep(INTANS(x)::ans_stack,gamma,ls,dump))
        | singleStep(ans_stack,gamma,OPBOOL(x)::ls,dump) = (print "opbool\n";singleStep(BOOLANS(x)::ans_stack,gamma,ls,dump))

        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPPLUS::ls,dump) = (print "opplus\n";singleStep(INTANS(a2+a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPSUB::ls,dump) = (print "opsub\n";singleStep(INTANS(a2-a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPTIMES::ls,dump) = (print "optimes\n";singleStep(INTANS(a2*a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPINTDIV::ls,dump) = (print "opintdiv";singleStep(INTANS(a2 div a1)::ans_stack,gamma,ls,dump))
        
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPGT::ls,dump) = (print "opgt\n";singleStep(BOOLANS(a2>a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPGTE::ls,dump) = (print "opgte\n";singleStep(BOOLANS(a2>=a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPLT::ls,dump) = (print "oplt\n";singleStep(BOOLANS(a2<a1)::ans_stack,gamma,ls,dump))
        | singleStep(INTANS(a1)::(INTANS(a2)::ans_stack),gamma,OPLTE::ls,dump) = (print "oplte\n";singleStep(BOOLANS(a2 <= a1)::ans_stack,gamma,ls,dump))
        | singleStep((a1)::((a2)::ans_stack),gamma,OPEQ::ls,dump) = (print "opeq\n";singleStep(BOOLANS(a2=a1)::ans_stack,gamma,ls,dump))
        | singleStep((a1)::((a2)::ans_stack),gamma,OPNEQ::ls,dump) = (print "opneq\n";singleStep(BOOLANS(a2<>a1)::ans_stack,gamma,ls,dump))

        | singleStep(BOOLANS(a1)::(BOOLANS(a2)::ans_stack),gamma,OPAND::ls,dump) = (print "opand\n";singleStep(BOOLANS(a2 andalso a1)::ans_stack,gamma,ls,dump))
        | singleStep(BOOLANS(a1)::(BOOLANS(a2)::ans_stack),gamma,OPOR::ls,dump) = (print "opor\n";singleStep(BOOLANS(a2 orelse a1)::ans_stack,gamma,ls,dump))
        | singleStep(BOOLANS(a1)::ans_stack,gamma,OPNOT::ls,dump) = (print "opnot\n";singleStep(BOOLANS(not(a1))::ans_stack,gamma,ls,dump))
(*
        | singleStep(BOOLANS(true)::a1::a2::ans_stack,gamma,OP_IF_THEN_ELSE::ls,dump) = (print "ifthenelse true\n";singleStep(a1::ans_stack,gamma,ls,dump))
        | singleStep(BOOLANS(false)::a1::a2::ans_stack,gamma,OP_IF_THEN_ELSE::ls,dump) = (print "ifthenelse false\n";singleStep(a2::ans_stack,gamma,ls,dump))*)


        | singleStep(BOOLANS(true)::ans_stack,gamma,IF::THEN_ELSE(a1,a2)::ls,dump) = (print "ifthenelse true\n";singleStep(ans_stack,gamma,a1@ls,dump))
        | singleStep(BOOLANS(false)::ans_stack,gamma,IF::THEN_ELSE(a1,a2)::ls,dump) = (print "ifthenelse false\n";singleStep(ans_stack,gamma,a2@ls,dump))

        | singleStep(ans_stack,gamma,OPLET::ls,dump) = singleStep(ans_stack,gamma,ls,(ans_stack,gamma,ls)::dump)
        | singleStep(ans_stack,gamma,OPBIND([OPVAR(x)],c2)::ls,dump) = singleStep(ans_stack,(VAR(x),List.hd(singleStep([],gamma,c2,[])))::gamma,ls,dump)
        | singleStep(ans_stack,gamma,OPIN::ls,dump) = singleStep(ans_stack,gamma,ls,dump)
        | singleStep(ans_stack,gamma,OPEND::ls,(a1,gamma1,c1)::dump) = singleStep(ans_stack,gamma1,ls,dump)


        | singleStep(ans_stack,gamma,OPLAMBDA(OPVAR(y),c1)::c2,dump) = (print "oplambda\n";singleStep(CLOSURE(gamma,OPVAR(y),c1)::ans_stack,gamma,c2,dump))
        | singleStep(a1::(CLOSURE(gamma1,OPVAR(x),c1)::ans_stack),gamma,OPAPP::c2,dump) = (print "opapp\n";singleStep([],(VAR(x),a1)::gamma1,c1,(ans_stack,gamma,c2)::dump))
        | singleStep(a::ans_stack1,gamma2,RET::c3,(ans_stack,gamma,c2)::dump) = (print "opret\n";singleStep(a::ans_stack,gamma,c2,dump))


  end