The lambda calculus consists of  

    variables
    abstractions (or functions) as first class expressions
    applications (of function calls)

In addition, one may admit any data types, and operations

    constants such as integers 
     built-in functions such as addition, multiplication, etc.
    other data types, including tuples.

One may also have conditionals, such as if-then-else.


[ Note: Theoretically, all the data types, conditionals etc can be encoded within the first three constructs. ]


In this assignment:  You have to implement an interpreter for the lambda-calculus, with a small extension to accommodate integers, and some basic operations (such as test for zero, addition, etc.)  so as to write some simple functions.

You can also include tuples and let-expressions if you wish (more credit)


Two different parameter-passing semantics need to be supported (two different parts of the assignment)

    Call-by-value:  write a compile function to the SECD machine, and encode the SECD machine in SML

    First, define a data type for OpCodes
    Next, a data type for closures
    Define a compile function from the source language to a sequence of OpCodes.
    Define the data structures for the SECD machine
    Define the one-step execution of the machine as an ML function
    Define execution by taking its transitive closure
    Define functions init and unload that initialize the machine with a compiled source program and on completion of execution, unload the machine, and return the result of execution. 


2. Call-by-name: implement the Krivine machine (extended to include arithmetic operations, and  appropriate closures).

Define a data type for closures
Define a function to initialize the machine with a given source program, 
Define the one-step execution of the Krivine machine
Define Machine execution by taking its transitive closure
Define a function to unload the result from the answer closure and return it as a lambda calculus answer.