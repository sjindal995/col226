functor CalcLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Calc_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(*val fact_rules : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goals : (Absyn.predicate list) list ref = ref [];
val fr_list : (Absyn.predicate * Absyn.predicate list) list ref = ref [];
val goal_list : (Absyn.predicate list) list ref = ref [];*)

end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\051\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\052\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\063\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\064\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\022\000\050\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\023\000\062\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\025\000\060\000\
\\032\000\014\000\033\000\013\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\032\000\014\000\
\\033\000\013\000\045\000\049\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\078\000\
\\022\000\078\000\023\000\078\000\025\000\078\000\026\000\078\000\
\\032\000\014\000\033\000\013\000\034\000\078\000\037\000\078\000\
\\045\000\078\000\046\000\078\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\079\000\
\\022\000\079\000\023\000\079\000\025\000\079\000\026\000\079\000\
\\032\000\014\000\033\000\013\000\034\000\079\000\037\000\079\000\
\\045\000\079\000\046\000\079\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\080\000\
\\022\000\080\000\023\000\080\000\025\000\080\000\026\000\080\000\
\\032\000\014\000\033\000\013\000\034\000\080\000\037\000\080\000\
\\045\000\080\000\046\000\080\000\000\000\
\\001\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\020\000\081\000\
\\022\000\081\000\023\000\081\000\025\000\081\000\026\000\081\000\
\\032\000\014\000\033\000\013\000\034\000\081\000\037\000\081\000\
\\045\000\081\000\046\000\081\000\000\000\
\\001\000\013\000\012\000\016\000\011\000\017\000\010\000\018\000\009\000\
\\019\000\008\000\021\000\007\000\024\000\006\000\027\000\005\000\
\\042\000\004\000\000\000\
\\001\000\016\000\027\000\017\000\026\000\000\000\
\\001\000\019\000\053\000\000\000\
\\001\000\019\000\054\000\000\000\
\\001\000\034\000\000\000\037\000\000\000\000\000\
\\001\000\035\000\046\000\000\000\
\\001\000\035\000\047\000\000\000\
\\001\000\046\000\048\000\000\000\
\\068\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\032\000\014\000\
\\033\000\013\000\000\000\
\\069\000\000\000\
\\070\000\000\000\
\\071\000\000\000\
\\072\000\000\000\
\\073\000\000\000\
\\074\000\003\000\023\000\004\000\022\000\019\000\015\000\000\000\
\\075\000\003\000\023\000\004\000\022\000\019\000\015\000\000\000\
\\076\000\019\000\015\000\000\000\
\\077\000\019\000\015\000\000\000\
\\082\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\019\000\015\000\032\000\014\000\033\000\013\000\000\000\
\\083\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\019\000\015\000\032\000\014\000\033\000\013\000\000\000\
\\084\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\019\000\015\000\032\000\014\000\033\000\013\000\000\000\
\\085\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\019\000\015\000\032\000\014\000\033\000\013\000\000\000\
\\086\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\019\000\015\000\032\000\014\000\033\000\013\000\000\000\
\\087\000\000\000\
\\088\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\032\000\014\000\
\\033\000\013\000\000\000\
\\089\000\000\000\
\\090\000\000\000\
\\091\000\000\000\
\\092\000\001\000\025\000\002\000\024\000\003\000\023\000\004\000\022\000\
\\006\000\021\000\007\000\020\000\008\000\019\000\009\000\018\000\
\\010\000\017\000\011\000\016\000\019\000\015\000\026\000\061\000\
\\032\000\014\000\033\000\013\000\000\000\
\\093\000\000\000\
\"
val actionRowNumbers =
"\012\000\020\000\013\000\012\000\
\\012\000\012\000\012\000\021\000\
\\022\000\023\000\024\000\012\000\
\\012\000\012\000\012\000\012\000\
\\012\000\012\000\012\000\012\000\
\\012\000\012\000\012\000\012\000\
\\017\000\018\000\032\000\019\000\
\\007\000\004\000\000\000\034\000\
\\033\000\001\000\031\000\030\000\
\\010\000\009\000\011\000\008\000\
\\029\000\028\000\027\000\026\000\
\\014\000\015\000\012\000\012\000\
\\012\000\025\000\039\000\012\000\
\\012\000\006\000\040\000\005\000\
\\002\000\003\000\035\000\012\000\
\\012\000\037\000\038\000\041\000\
\\036\000\016\000"
val gotoT =
"\
\\001\000\065\000\002\000\001\000\000\000\
\\000\000\
\\000\000\
\\002\000\026\000\000\000\
\\002\000\028\000\003\000\027\000\000\000\
\\002\000\029\000\000\000\
\\002\000\030\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\031\000\000\000\
\\002\000\032\000\000\000\
\\002\000\033\000\000\000\
\\002\000\034\000\000\000\
\\002\000\035\000\000\000\
\\002\000\036\000\000\000\
\\002\000\037\000\000\000\
\\002\000\038\000\000\000\
\\002\000\039\000\000\000\
\\002\000\040\000\000\000\
\\002\000\041\000\000\000\
\\002\000\042\000\000\000\
\\002\000\043\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\053\000\000\000\
\\002\000\054\000\000\000\
\\002\000\055\000\000\000\
\\000\000\
\\000\000\
\\002\000\056\000\000\000\
\\002\000\057\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\002\000\028\000\003\000\063\000\000\000\
\\002\000\064\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\"
val numstates = 66
val numrules = 26
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle General.Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(List.map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit ->  unit
 | NUM of unit ->  (int) | CAPID of unit ->  (string)
 | SMALLID of unit ->  (string) | STRING of unit ->  (string)
 | REAL of unit ->  (real) | BOOL of unit ->  (bool)
 | def_seq of unit ->  (Absyn.exp list)
 | expression of unit ->  (Absyn.exp)
 | program of unit ->  (Absyn.exp)
end
type svalue = MlyValue.svalue
type result = Absyn.exp
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn (T 35) => true | (T 36) => true | _ => false
val preferred_change : (term list * term list) list = 
(nil
 $$ (T 15),nil
 $$ (T 35))::
(nil
 $$ (T 16),nil
 $$ (T 35))::
nil
val noShift = 
fn (T 33) => true | _ => false
val showTerminal =
fn (T 0) => "PLUS"
  | (T 1) => "SUB"
  | (T 2) => "TIMES"
  | (T 3) => "INTDIV"
  | (T 4) => "REALDIV"
  | (T 5) => "GREAT"
  | (T 6) => "LESSTHAN"
  | (T 7) => "GREATEQ"
  | (T 8) => "LESSEQ"
  | (T 9) => "EQ"
  | (T 10) => "NEQ"
  | (T 11) => "CAT"
  | (T 12) => "BOOL"
  | (T 13) => "REAL"
  | (T 14) => "STRING"
  | (T 15) => "SMALLID"
  | (T 16) => "CAPID"
  | (T 17) => "NUM"
  | (T 18) => "LPAREN"
  | (T 19) => "RPAREN"
  | (T 20) => "IF"
  | (T 21) => "THEN"
  | (T 22) => "ELSE"
  | (T 23) => "LET"
  | (T 24) => "END"
  | (T 25) => "COMMA"
  | (T 26) => "NEGATION"
  | (T 27) => "COLON"
  | (T 28) => "MOD"
  | (T 29) => "ABS"
  | (T 30) => "EXPONENT"
  | (T 31) => "CONJUNCT"
  | (T 32) => "DISJUNCT"
  | (T 33) => "EOF"
  | (T 34) => "DOT"
  | (T 35) => "PRINT"
  | (T 36) => "SEMI"
  | (T 37) => "QUES"
  | (T 38) => "LSBRACKET"
  | (T 39) => "RSBRACKET"
  | (T 40) => "LISTBAR"
  | (T 41) => "LAMBDA"
  | (T 42) => "FUNCTION"
  | (T 43) => "FI"
  | (T 44) => "COLON_EQ"
  | (T 45) => "IN"
  | (T 46) => "EQUAL_SIGN"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 46) $$ (T 45) $$ (T 44) $$ (T 43) $$ (T 42) $$ (T 41) $$ (T 40)
 $$ (T 39) $$ (T 38) $$ (T 37) $$ (T 36) $$ (T 35) $$ (T 34) $$ (T 33)
 $$ (T 32) $$ (T 31) $$ (T 30) $$ (T 29) $$ (T 28) $$ (T 27) $$ (T 26)
 $$ (T 25) $$ (T 24) $$ (T 23) $$ (T 22) $$ (T 21) $$ (T 20) $$ (T 19)
 $$ (T 18) $$ (T 11) $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ 
(T 5) $$ (T 4) $$ (T 3) $$ (T 2) $$ (T 1) $$ (T 0)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.expression expression1, expression1left, 
expression1right)) :: rest671)) => let val  result = MlyValue.program
 (fn _ => let val  (expression as expression1) = expression1 ()
 in (expression)
end)
 in ( LrTable.NT 0, ( result, expression1left, expression1right), 
rest671)
end
|  ( 1, ( ( _, ( MlyValue.NUM NUM1, NUM1left, NUM1right)) :: rest671))
 => let val  result = MlyValue.expression (fn _ => let val  (NUM as 
NUM1) = NUM1 ()
 in (Absyn.NUM NUM)
end)
 in ( LrTable.NT 1, ( result, NUM1left, NUM1right), rest671)
end
|  ( 2, ( ( _, ( MlyValue.CAPID CAPID1, CAPID1left, CAPID1right)) :: 
rest671)) => let val  result = MlyValue.expression (fn _ => let val  (
CAPID as CAPID1) = CAPID1 ()
 in (Absyn.VAR CAPID)
end)
 in ( LrTable.NT 1, ( result, CAPID1left, CAPID1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.SMALLID SMALLID1, SMALLID1left, 
SMALLID1right)) :: rest671)) => let val  result = MlyValue.expression
 (fn _ => let val  (SMALLID as SMALLID1) = SMALLID1 ()
 in (Absyn.VAR SMALLID)
end)
 in ( LrTable.NT 1, ( result, SMALLID1left, SMALLID1right), rest671)

end
|  ( 4, ( ( _, ( MlyValue.BOOL BOOL1, BOOL1left, BOOL1right)) :: 
rest671)) => let val  result = MlyValue.expression (fn _ => let val  (
BOOL as BOOL1) = BOOL1 ()
 in (Absyn.BOOL BOOL)
end)
 in ( LrTable.NT 1, ( result, BOOL1left, BOOL1right), rest671)
end
|  ( 5, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.expression 
expression1, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let
 val  result = MlyValue.expression (fn _ => let val  (expression as 
expression1) = expression1 ()
 in (expression)
end)
 in ( LrTable.NT 1, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.expression expression2, _, expression2right)
) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.PLUS (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 7, ( ( _, ( MlyValue.expression expression2, _, expression2right)
) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.SUB (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 8, ( ( _, ( MlyValue.expression expression2, _, expression2right)
) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.TIMES (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 9, ( ( _, ( MlyValue.expression expression2, _, expression2right)
) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.INTDIV (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 10, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.GT (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 11, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.GTE (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 12, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.LTE (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 13, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.LT (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 14, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.EQ (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 15, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.NEQ (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 16, ( ( _, ( MlyValue.expression expression1, _, expression1right
)) :: ( _, ( _, NEGATION1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  expression1 = expression1 ()
 in (Absyn.NOT (expression1))
end)
 in ( LrTable.NT 1, ( result, NEGATION1left, expression1right), 
rest671)
end
|  ( 17, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.AND (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 18, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.OR (expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, expression2right), 
rest671)
end
|  ( 19, ( ( _, ( _, _, END1right)) :: ( _, ( MlyValue.expression 
expression1, _, _)) :: _ :: ( _, ( MlyValue.def_seq def_seq1, _, _))
 :: ( _, ( _, LET1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (def_seq as def_seq1) = def_seq1
 ()
 val  (expression as expression1) = expression1 ()
 in (Absyn.LET_IN_END(def_seq,expression))
end)
 in ( LrTable.NT 1, ( result, LET1left, END1right), rest671)
end
|  ( 20, ( ( _, ( MlyValue.expression expression3, _, expression3right
)) :: _ :: ( _, ( MlyValue.expression expression2, _, _)) :: _ :: ( _,
 ( MlyValue.expression expression1, _, _)) :: ( _, ( _, IF1left, _))
 :: rest671)) => let val  result = MlyValue.expression (fn _ => let
 val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 val  expression3 = expression3 ()
 in (Absyn.IF_THEN_ELSE(expression1,expression2,expression3))
end)
 in ( LrTable.NT 1, ( result, IF1left, expression3right), rest671)
end
|  ( 21, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.expression 
expression1, _, _)) :: _ :: _ :: ( _, ( MlyValue.CAPID CAPID1, _, _))
 :: ( _, ( _, LAMBDA1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (CAPID as CAPID1) = CAPID1 ()
 val  (expression as expression1) = expression1 ()
 in (Absyn.LAMBDA(CAPID,expression))
end)
 in ( LrTable.NT 1, ( result, LAMBDA1left, RPAREN1right), rest671)
end
|  ( 22, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.expression 
expression1, _, _)) :: _ :: _ :: ( _, ( MlyValue.SMALLID SMALLID1, _,
 _)) :: ( _, ( _, LAMBDA1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  (SMALLID as SMALLID1) = SMALLID1
 ()
 val  (expression as expression1) = expression1 ()
 in (Absyn.LAMBDA(SMALLID,expression))
end)
 in ( LrTable.NT 1, ( result, LAMBDA1left, RPAREN1right), rest671)
end
|  ( 23, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.expression 
expression2, _, _)) :: _ :: ( _, ( MlyValue.expression expression1, 
expression1left, _)) :: rest671)) => let val  result = 
MlyValue.expression (fn _ => let val  expression1 = expression1 ()
 val  expression2 = expression2 ()
 in (Absyn.APP(expression1,expression2))
end)
 in ( LrTable.NT 1, ( result, expression1left, RPAREN1right), rest671)

end
|  ( 24, ( ( _, ( MlyValue.expression expression2, _, expression2right
)) :: _ :: ( _, ( MlyValue.expression expression1, expression1left, _)
) :: rest671)) => let val  result = MlyValue.def_seq (fn _ => let val 
 expression1 = expression1 ()
 val  expression2 = expression2 ()
 in ([Absyn.BIND(expression1,expression2)])
end)
 in ( LrTable.NT 2, ( result, expression1left, expression2right), 
rest671)
end
|  ( 25, ( ( _, ( MlyValue.def_seq def_seq1, _, def_seq1right)) :: _
 :: ( _, ( MlyValue.expression expression2, _, _)) :: _ :: ( _, ( 
MlyValue.expression expression1, expression1left, _)) :: rest671)) =>
 let val  result = MlyValue.def_seq (fn _ => let val  expression1 = 
expression1 ()
 val  expression2 = expression2 ()
 val  (def_seq as def_seq1) = def_seq1 ()
 in (Absyn.BIND(expression1,expression2)::def_seq)
end)
 in ( LrTable.NT 2, ( result, expression1left, def_seq1right), rest671
)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.program x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a ()
end
end
structure Tokens : Calc_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(
ParserData.MlyValue.VOID,p1,p2))
fun SUB (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(
ParserData.MlyValue.VOID,p1,p2))
fun TIMES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(
ParserData.MlyValue.VOID,p1,p2))
fun INTDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(
ParserData.MlyValue.VOID,p1,p2))
fun REALDIV (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(
ParserData.MlyValue.VOID,p1,p2))
fun GREAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(
ParserData.MlyValue.VOID,p1,p2))
fun LESSTHAN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(
ParserData.MlyValue.VOID,p1,p2))
fun GREATEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(
ParserData.MlyValue.VOID,p1,p2))
fun LESSEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(
ParserData.MlyValue.VOID,p1,p2))
fun EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(
ParserData.MlyValue.VOID,p1,p2))
fun NEQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(
ParserData.MlyValue.VOID,p1,p2))
fun CAT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 11,(
ParserData.MlyValue.VOID,p1,p2))
fun BOOL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 12,(
ParserData.MlyValue.BOOL (fn () => i),p1,p2))
fun REAL (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 13,(
ParserData.MlyValue.REAL (fn () => i),p1,p2))
fun STRING (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 14,(
ParserData.MlyValue.STRING (fn () => i),p1,p2))
fun SMALLID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 15,(
ParserData.MlyValue.SMALLID (fn () => i),p1,p2))
fun CAPID (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 16,(
ParserData.MlyValue.CAPID (fn () => i),p1,p2))
fun NUM (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 17,(
ParserData.MlyValue.NUM (fn () => i),p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 18,(
ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 19,(
ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 20,(
ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 21,(
ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 22,(
ParserData.MlyValue.VOID,p1,p2))
fun LET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 23,(
ParserData.MlyValue.VOID,p1,p2))
fun END (p1,p2) = Token.TOKEN (ParserData.LrTable.T 24,(
ParserData.MlyValue.VOID,p1,p2))
fun COMMA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 25,(
ParserData.MlyValue.VOID,p1,p2))
fun NEGATION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 26,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON (p1,p2) = Token.TOKEN (ParserData.LrTable.T 27,(
ParserData.MlyValue.VOID,p1,p2))
fun MOD (p1,p2) = Token.TOKEN (ParserData.LrTable.T 28,(
ParserData.MlyValue.VOID,p1,p2))
fun ABS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 29,(
ParserData.MlyValue.VOID,p1,p2))
fun EXPONENT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 30,(
ParserData.MlyValue.VOID,p1,p2))
fun CONJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 31,(
ParserData.MlyValue.VOID,p1,p2))
fun DISJUNCT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 32,(
ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 33,(
ParserData.MlyValue.VOID,p1,p2))
fun DOT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 34,(
ParserData.MlyValue.VOID,p1,p2))
fun PRINT (p1,p2) = Token.TOKEN (ParserData.LrTable.T 35,(
ParserData.MlyValue.VOID,p1,p2))
fun SEMI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 36,(
ParserData.MlyValue.VOID,p1,p2))
fun QUES (p1,p2) = Token.TOKEN (ParserData.LrTable.T 37,(
ParserData.MlyValue.VOID,p1,p2))
fun LSBRACKET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 38,(
ParserData.MlyValue.VOID,p1,p2))
fun RSBRACKET (p1,p2) = Token.TOKEN (ParserData.LrTable.T 39,(
ParserData.MlyValue.VOID,p1,p2))
fun LISTBAR (p1,p2) = Token.TOKEN (ParserData.LrTable.T 40,(
ParserData.MlyValue.VOID,p1,p2))
fun LAMBDA (p1,p2) = Token.TOKEN (ParserData.LrTable.T 41,(
ParserData.MlyValue.VOID,p1,p2))
fun FUNCTION (p1,p2) = Token.TOKEN (ParserData.LrTable.T 42,(
ParserData.MlyValue.VOID,p1,p2))
fun FI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 43,(
ParserData.MlyValue.VOID,p1,p2))
fun COLON_EQ (p1,p2) = Token.TOKEN (ParserData.LrTable.T 44,(
ParserData.MlyValue.VOID,p1,p2))
fun IN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 45,(
ParserData.MlyValue.VOID,p1,p2))
fun EQUAL_SIGN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 46,(
ParserData.MlyValue.VOID,p1,p2))
end
end
